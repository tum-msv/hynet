# Contributing

Thank you for considering to contribute to *hynet*, your support is very welcome! Please report any issues with this package using the [issue tracker](https://gitlab.com/tum-msv/hynet/issues). You may use the [code snippets](https://gitlab.com/tum-msv/hynet/snippets) to discuss and share applications of *hynet*. For contributions to the code and documentation, please consider the following contribution guidelines.

## Scope

*hynet* aims to offer an easy-to-use, robust, and extensible optimal power flow (OPF) framework for hybrid AC/DC grids with point-to-point and radial multi-terminal HVDC systems. At this point, its primary focus is to provide a convenient platform for OPF studies as well as a basis for extensions (e.g. model extensions, security-constrained OPF, multi-period OPF, etc.), while the extensions themselves may be better situated in separate packages to keep *hynet*'s code base concise and accessible. All contributions that support *hynet*'s mission, including e.g. additional interfaces to solvers and further relaxation methods, are highly welcome!

## Coding Style

In *hynet*, we strive to maintain a clearly structured code base that is well comprehensible. Besides trying to keep things as simple as possible, the code further promotes readability by generally following the [PEP 8](https://www.python.org/dev/peps/pep-0008/) coding style guide and consistently providing documentation using [NumPy-style doctrings](https://numpydoc.readthedocs.io/en/latest/format.html). Please support this goal by staying consistent with these conventions in contributions to *hynet*.

## Contributing Code

A contribution may be proposed and described by opening a new [issue](https://gitlab.com/tum-msv/hynet/issues) for the targeted feature. To implement the feature, please [fork](https://docs.gitlab.com/ce/workflow/forking_workflow.html) the *hynet* repository, update the code, add the corresponding [unit tests](https://docs.python.org/3/library/unittest.html), update ``CHANGELOG.md`` with the staged feature, and create a [merge request](https://docs.gitlab.com/ce/workflow/forking_workflow.html#merging-upstream) for *hynet*'s master branch. By submitting the merge request, you consent that your contribution is licensed under the [BSD 3-clause license](https://gitlab.com/tum-msv/hynet/blob/master/LICENSE) that covers the project.

**Thank you very much for your time and effort to support *hynet*!**
