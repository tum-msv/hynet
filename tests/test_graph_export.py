import unittest
import json

from hynet.test.system import TEST_SYSTEM
from hynet.visual.graph import (export_networkx_graph_to_json,
                                create_networkx_graph)
from tests.test_utilities import remove_temporary_file

OUTPUT_FILE = "_hynet_test_graph.json"


class GraphExportTest(unittest.TestCase):

    def setUp(self):
        remove_temporary_file(OUTPUT_FILE)

    def tearDown(self):
        remove_temporary_file(OUTPUT_FILE)

    def test(self):
        export_networkx_graph_to_json(create_networkx_graph(TEST_SYSTEM),
                                      output_file=OUTPUT_FILE)

        with open(OUTPUT_FILE, 'r') as file:
            graph = json.load(file)

        self.assertEqual(len(graph['nodes']),
                         11 + 6,  # Buses and injectors
                         "Incorrect number of nodes in graph output.")
        self.assertEqual(len(graph['links']),
                         7 + 3 + 6,  # Corridors, AC/DC converters, injectors
                         "Incorrect number of links in graph output.")

        self.assertEqual(graph['nodes'][0]['id'], 'Bus 1',
                         "Incorrect node ID for first node in graph.")

        self.assertEqual(graph['nodes'][0]['attr_dict']['id'], 1,
                         "Incorrect bus ID for first node in graph.")

        self.assertEqual(graph['nodes'][0]['attr_dict']['graph_type'], 'bus',
                         "Incorrect graph type for first node in graph.")

        self.assertEqual(graph['nodes'][0]['attr_dict']['type'], 'AC',
                         "Incorrect bus type for first node in graph.")

        for element in graph['nodes']:
            self.examine_graph_element(element)

        for element in graph['links']:
            self.examine_graph_element(element)

    def examine_graph_element(self, element):
        self.assertIsNotNone(element['attr_dict'],
                             "Missing attribute dictionary on graph element.")
        attr_dict = element['attr_dict']
        graph_type = attr_dict['graph_type']

        if graph_type in ['bus', 'injector']:
            self.assertIsNotNone(element['id'], "Missing ID of graph node.")
            self.assertTrue(element['id'].find(str(attr_dict['id'])) >= 0,
                            "Node ID does not contain the entity ID.")
            self.assertTrue(element['id'].lower().find(graph_type) >= 0,
                            "Node ID does not contain the graph type.")
        elif graph_type in ['branch', 'converter', 'terminal']:
            self.assertIsNotNone(element['source'], "Link without source.")
            self.assertIsNotNone(element['target'], "Link without target.")
        else:
            self.fail("Invalid graph type '" + graph_type + "' encountered.")
