import logging
import unittest

import hynet as ht
from hynet.test.system import TEST_SYSTEM

_log = logging.getLogger(__name__)


class ScenarioTest(unittest.TestCase):

    def setUp(self):
        self._scenario = TEST_SYSTEM.copy()

    def tearDown(self):
        pass

    def test_get_ac_subgrids(self):
        subgrid_list = self._scenario.get_ac_subgrids()
        self.assertTrue(len(subgrid_list) == 2)
        self.assertTrue(all(subgrid_list[0] == [1, 2, 4, 5, 3]))
        self.assertTrue(all(subgrid_list[1] == [9, 10]))

    def test_get_dc_subgrids(self):
        subgrid_list = self._scenario.get_dc_subgrids()
        self.assertTrue(len(subgrid_list) == 2)
        self.assertTrue(all(subgrid_list[0] == [6, 7, 8]))
        self.assertTrue(all(subgrid_list[1] == [11]))

    def test_get_islands(self):
        island_list = self._scenario.get_islands()
        self.assertTrue(len(island_list) == 3)
        self.assertTrue(all(island_list[0] == [1, 2, 4, 5, 3, 6, 7, 8]))
        self.assertTrue(all(island_list[1] == [9, 10]))
        self.assertTrue(all(island_list[2] == [11]))

    def test_get_parallel_branches(self):
        branch_list = self._scenario.get_parallel_branches()
        self.assertTrue(len(branch_list) == 3)
        self.assertTrue(all(branch_list[0] == [2, -2]))
        self.assertTrue(all(branch_list[1] == [7, -3]))
        self.assertTrue(all(branch_list[2] == [5, -1]))

    def test_get_ac_branches(self):
        branches = self._scenario.get_ac_branches()
        self.assertTrue(all(branches == [1, 2, 3, 4, 5, -1, -2]))

    def test_get_dc_branches(self):
        branches = self._scenario.get_dc_branches()
        self.assertTrue(all(branches == [7, 6, -3]))

    def test_remove_buses(self):
        branches, converters, injectors = self._scenario.remove_buses([1, 10, 11])
        self.assertTrue(all(self._scenario.bus.index == [2, 3, 4, 5, 6, 7, 8, 9]))
        self.assertTrue(all(branches == [1, 2, 3, 5, -1, -2]))
        self.assertTrue(all(converters == [4]))
        self.assertTrue(all(injectors == [1, 5, 6]))

    def test_minimum_series_resistance(self):
        r_bar = self._scenario.branch['z_bar'].to_numpy().real.copy()
        x_bar = self._scenario.branch['z_bar'].to_numpy().imag.copy()
        r_bar_min = 2*r_bar.min()
        mask = self._scenario.branch['z_bar'].to_numpy().real > r_bar_min
        self.assertFalse(mask.all())
        self._scenario.set_minimum_series_resistance(r_bar_min)
        z_bar_new = self._scenario.branch['z_bar'].to_numpy()
        self.assertTrue(z_bar_new.real.min() == r_bar_min,
                        "The minimum series resistance was not set properly.")
        self.assertTrue(all(z_bar_new.real[mask] == r_bar[mask]),
                        "The minimum series resistance was not set properly.")
        self.assertTrue(all(z_bar_new.imag == x_bar),
                        "The series reactance was modified.")

    def test_ensure_reference(self):
        self._scenario.bus['ref'] = False
        self._scenario.ensure_reference()
        reference_buses = self._scenario.bus.index[self._scenario.bus['ref']]
        self.assertTrue(all(reference_buses == [5, 10]),
                        "The reference bus was not set properly.")

    def test_add_entities(self):
        scenario = ht.Scenario()
        scenario.base_mva = 100

        scenario.add_bus(type_=ht.BusType.AC,
                         base_kv=220,
                         v_min=0.9,
                         v_max=1.1,
                         ref=True,
                         y_tld=0.02,
                         load=8,
                         zone=1)

        scenario.add_injector(type_=ht.InjectorType.CONVENTIONAL,
                              bus=1,
                              cap=ht.CapRegion(p_bnd=(2, 50),
                                               q_bnd=(-40, 40)),
                              cost_p=ht.PWLFunction(marginal_price=25))

        scenario.add_bus(type_=ht.BusType.AC,
                         base_kv=220,
                         v_min=0.95,
                         v_max=1.05,
                         y_tld=0.015,
                         load=5.5,
                         zone=2)

        scenario.add_branch(type_=ht.BranchType.LINE,
                            src=1,
                            dst=2,
                            z_bar=0.001+0.005j,
                            y_src=0.00001j,
                            y_dst=0.00002j,
                            rho_src=1.01,
                            rho_dst=1/1.01,
                            length=10,
                            rating=100,
                            angle_min=-20,
                            angle_max=+30,
                            drop_min=-8,
                            drop_max=12)

        scenario.add_converter(src=2,
                               dst=1,
                               cap_src=ht.ConverterCapRegion(p_bnd=(-10, 10),
                                                             q_bnd=(-5, 5)),
                               cap_dst=ht.ConverterCapRegion(p_bnd=(-10, 10)),
                               loss_fwd=0.075,
                               loss_bwd=0.005,
                               loss_fix=4)

        scenario.add_compensator(bus=2, q_max=5)

        scenario.verify()

        self.assertTrue(scenario.num_buses == 2)
        self.assertTrue(scenario.num_branches == 1)
        self.assertTrue(scenario.num_converters == 1)
        self.assertTrue(scenario.num_injectors == 2)

        self.assertTrue((scenario.bus.index == [1, 2]).all())
        self.assertTrue((scenario.branch.index == [1]).all())
        self.assertTrue((scenario.converter.index == [1]).all())
        self.assertTrue((scenario.injector.index == [1, 2]).all())
