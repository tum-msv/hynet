import os

import hynet as ht
from hynet.data.connection import DBTransaction
from hynet.data.structure import (SCHEMA_VERSION,
                                  DBInfo,
                                  DBBus,
                                  DBShunt,
                                  DBBranch,
                                  DBScenario,
                                  DBCapabilityRegion,
                                  DBSamplePoint,
                                  DBInjector,
                                  DBConverter,
                                  DBScenarioLoad)
from hynet.types_ import DBInfoKey


def remove_temporary_file(db_path):
    if os.path.isfile(db_path):
        os.remove(db_path)


def create_sample_database(file_name):
    database = ht.connect(file_name)
    with DBTransaction(database) as transaction:
        version = DBInfo(key=DBInfoKey.VERSION, value=SCHEMA_VERSION)
        grid_name = DBInfo(key=DBInfoKey.GRID_NAME, value='Test grid')
        base_mva = DBInfo(key=DBInfoKey.BASE_MVA, value='30')
        description = DBInfo(key=DBInfoKey.DESCRIPTION, value='')
        transaction.update(version)
        transaction.update(grid_name)
        transaction.update(base_mva)
        transaction.update(description)
    # Insert a dummy bus for test
    with DBTransaction(database) as transaction:
        test_bus_1 = DBBus()
        test_bus_1.type = ht.BusType.AC
        test_bus_1.ref = False
        test_bus_1.base_kv = 1.0
        test_bus_1.v_min = 42
        test_bus_1.v_max = 1337
        test_bus_1.annotation = "This is the test bus!"
        transaction.add(test_bus_1)
    with DBTransaction(database) as transaction:
        test_bus_2 = DBBus()
        test_bus_2.type = ht.BusType.AC
        test_bus_2.ref = True
        test_bus_2.base_kv = 1.0
        test_bus_2.v_min = 4319
        test_bus_2.v_max = 69000
        test_bus_2.annotation = "This is the second test bus!"
        transaction.add(test_bus_2)
    with DBTransaction(database) as transaction:
        test_bus_3 = DBBus()
        test_bus_3.type = ht.BusType.DC
        test_bus_3.ref = False
        test_bus_3.base_kv = 1.1
        test_bus_3.v_min = 7321
        test_bus_3.v_max = 14121
        test_bus_3.annotation = "This is the third test bus!"
        transaction.add(test_bus_3)

    with DBTransaction(database) as transaction:
        test_shunt = DBShunt()
        test_shunt.bus = test_bus_1
        test_shunt.p = 123
        test_shunt.q = 321
        test_shunt.annotation = "This is the test shunt!"
        transaction.add(test_shunt)

    with DBTransaction(database) as transaction:
        test_shunt_2 = DBShunt()
        test_shunt_2.bus = test_bus_2
        test_shunt_2.p = 234
        test_shunt_2.q = 432
        test_shunt_2.annotation = "This is the second test shunt!"
        transaction.add(test_shunt_2)

    with DBTransaction(database) as transaction:
        test_branch = DBBranch()
        test_branch.type = ht.BranchType.LINE
        test_branch.src = test_bus_1
        test_branch.dst = test_bus_2
        test_branch.r = 10
        test_branch.x = 13
        test_branch.b_src = 0.0005
        test_branch.b_dst = 0.0005
        test_branch.ratio_src = 3
        test_branch.phase_src = 1
        test_branch.ratio_dst = 3
        test_branch.phase_dst = 1
        test_branch.length = 42
        test_branch.annotation = "This is the test branch!"
        transaction.add(test_branch)
    with DBTransaction(database) as transaction:
        test_scenario = DBScenario()
        test_scenario.name = "Test Scenario"
        test_scenario.time = 1337.42
        test_scenario.loss_price = 3.14
        test_scenario.annotation = "This is a test scenario!"
        transaction.add(test_scenario)

        # Scenario Load
        test_scenario_load_1 = DBScenarioLoad()
        test_scenario_load_1.scenario = test_scenario
        test_scenario_load_1.bus = test_bus_1
        test_scenario_load_1.p = 0.01
        test_scenario_load_1.q = 0.01
        transaction.add(test_scenario_load_1)

        test_scenario_load_2 = DBScenarioLoad()
        test_scenario_load_2.scenario = test_scenario
        test_scenario_load_2.bus = test_bus_2
        test_scenario_load_2.p = 0.02
        test_scenario_load_2.q = 0.02
        transaction.add(test_scenario_load_2)

        test_scenario_load_3 = DBScenarioLoad()
        test_scenario_load_3.scenario = test_scenario
        test_scenario_load_3.bus = test_bus_3
        test_scenario_load_3.p = 0.05
        test_scenario_load_3.q = 0.0
        transaction.add(test_scenario_load_3)

    with DBTransaction(database) as transaction:
        test_scenario_2 = DBScenario()
        test_scenario_2.name = "Test Scenario 2"
        test_scenario_2.time = 2664.84
        test_scenario_2.loss_price = 6.28
        test_scenario_2.annotation = "This is the second test scenario!"
        transaction.add(test_scenario_2)

        # Scenario Load
        test_scenario_load_1 = DBScenarioLoad()
        test_scenario_load_1.scenario = test_scenario_2
        test_scenario_load_1.bus = test_bus_1
        test_scenario_load_1.p = 0.01
        test_scenario_load_1.q = 0.01
        transaction.add(test_scenario_load_1)

        test_scenario_load_2 = DBScenarioLoad()
        test_scenario_load_2.scenario = test_scenario_2
        test_scenario_load_2.bus = test_bus_2
        test_scenario_load_2.p = 0.04
        test_scenario_load_2.q = 0.04
        transaction.add(test_scenario_load_2)

        test_scenario_load_3 = DBScenarioLoad()
        test_scenario_load_3.scenario = test_scenario_2
        test_scenario_load_3.bus = test_bus_3
        test_scenario_load_3.p = 0.06
        test_scenario_load_3.q = 0.0
        transaction.add(test_scenario_load_3)

    with DBTransaction(database) as transaction:
        test_capability_region = DBCapabilityRegion()
        test_capability_region.p_min = -1
        test_capability_region.q_min = -1
        test_capability_region.p_max = 1
        test_capability_region.q_max = 1

        test_capability_region.lb_ofs = 0.5
        test_capability_region.lb_slp = -1
        test_capability_region.lt_ofs = 0.5
        test_capability_region.lt_slp = 1
        test_capability_region.rb_ofs = 0.5
        test_capability_region.rb_slp = 1
        test_capability_region.rt_ofs = 0.5
        test_capability_region.rt_slp = -1
        test_capability_region.annotation = "This is a test annotation!"

        transaction.add(test_capability_region)

        test_capability_region_2 = DBCapabilityRegion()
        test_capability_region_2.p_min = -100000
        test_capability_region_2.q_min = 0
        test_capability_region_2.p_max = 100000
        test_capability_region_2.q_max = 0

        test_capability_region_2.lb_ofs = 0.5
        test_capability_region_2.lb_slp = -1
        test_capability_region_2.lt_ofs = 0.5
        test_capability_region_2.lt_slp = 1
        test_capability_region_2.rb_ofs = 0.5
        test_capability_region_2.rb_slp = 1
        test_capability_region_2.rt_ofs = 0.5
        test_capability_region_2.rt_slp = -1
        test_capability_region_2.annotation = "This is a test annotation!"

        transaction.add(test_capability_region_2)

        sample_point_1 = DBSamplePoint()
        sample_point_1.id = 1
        sample_point_1.x = 1
        sample_point_1.y = 0

        sample_point_2 = DBSamplePoint()
        sample_point_2.id = 1
        sample_point_2.x = 3
        sample_point_2.y = 5

        transaction.add(sample_point_1)
        transaction.add(sample_point_2)

        test_injector = DBInjector()
        test_injector.type = ht.InjectorType.LOAD
        test_injector.bus = test_bus_2
        test_injector.cap = test_capability_region
        test_injector.cost_p_id = 1
        test_injector.cost_q_id = 1
        test_injector.cost_start = 4321
        test_injector.cost_stop = 1234
        test_injector.annotation = "This ia a test injector!"

        transaction.add(test_injector)

        test_converter = DBConverter()
        test_converter.src = test_bus_2
        test_converter.dst = test_bus_3
        test_converter.cap_src = test_capability_region
        test_converter.cap_dst = test_capability_region_2
        test_converter.loss_fwd = 2
        test_converter.loss_bwd = 3
        test_converter.loss_fix = 3
        test_converter.annotation = "This is a test converter!"

        transaction.add(test_converter)
