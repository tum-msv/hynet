import logging
import unittest

import numpy as np

import hynet as ht
from hynet.test.system import TEST_SYSTEM
from hynet.expansion.selection import (get_series_resistance_weights,
                                       get_mst_branches,
                                       get_branches_outside_mst)

_log = logging.getLogger(__name__)


class ExpansionTest(unittest.TestCase):

    def setUp(self):
        scenario = TEST_SYSTEM.copy()
        scenario.remove_buses([9, 10, 11])
        scenario.converter.drop(scenario.converter.index,
                                inplace=True)

        def map_dc_buses(bus_id):
            if 6 <= bus_id <= 8:
                return bus_id - 3
            return bus_id

        scenario.branch['src'] = scenario.branch['src'].map(map_dc_buses)
        scenario.branch['dst'] = scenario.branch['dst'].map(map_dc_buses)

        scenario.remove_buses([6, 7, 8])

        r = scenario.branch['z_bar'].to_numpy().real
        x = scenario.branch['z_bar'].to_numpy().imag
        x[x == 0] = 10 * r[x == 0]
        scenario.branch.loc[:, 'z_bar'] = r + 1j*x

        scenario.branch.loc[-3, ['src', 'dst']] = \
            scenario.branch.loc[-3, ['dst', 'src']].to_numpy()

        scenario.verify()
        self.assertTrue(scenario.num_buses == 5)
        self.assertTrue(scenario.num_branches == 8)
        self.assertTrue(scenario.num_converters == 0)
        self.assertTrue(scenario.num_injectors == 3)
        self.assertTrue(len(scenario.get_islands()) == 1)
        self.assertTrue(len(scenario.get_dc_subgrids()) == 0)
        self.assertFalse(scenario.has_acyclic_ac_subgrids())

        self.scenario = scenario

    def tearDown(self):
        self.scenario = None

    def test_convert_transformer_to_b2b_converter(self):
        scenario = self.scenario.copy()
        q_to_p_ratio = 0.4
        loss_dyn = 1.9
        capacity_factor = 1.0

        # Without capacity uprating
        scenario.branch.at[3, 'annotation'] = '42 is weird'
        branch = scenario.branch.loc[3].copy()  # Convert branch 3
        converter_id, amalgamated = \
            ht.convert_transformer_to_b2b_converter(
                scenario,
                branch_id=branch.name,
                loss_dyn=loss_dyn,
                q_to_p_ratio=q_to_p_ratio,
                capacity_factor=capacity_factor,
                amalgamate=True
            )
        self.assertTrue(converter_id == 1)
        self.assertFalse(amalgamated)
        self.verify_acac_converter(scenario, converter_id, branch, q_to_p_ratio,
                                   loss_dyn, capacity_factor * branch['rating'],
                                   amalgamated)

        # With capacity uprating
        capacity_factor = np.sqrt(2)
        branch = scenario.branch.loc[1].copy()  # Convert branch 1
        converter_id, amalgamated = \
            ht.convert_transformer_to_b2b_converter(
                scenario,
                branch_id=branch.name,
                loss_dyn=loss_dyn,
                q_to_p_ratio=q_to_p_ratio,
                capacity_factor=capacity_factor,
                amalgamate=True
            )
        self.assertTrue(converter_id == 2)
        self.assertFalse(amalgamated)
        self.verify_acac_converter(scenario, converter_id, branch, q_to_p_ratio,
                                   loss_dyn, capacity_factor * branch['rating'],
                                   amalgamated)

    def test_convert_transformer_to_b2b_converter_without_rating(self):
        scenario = self.scenario.copy()
        scenario.branch.at[3, 'rating'] = np.nan
        with self.assertRaises(ValueError):
            ht.convert_transformer_to_b2b_converter(
                scenario,
                branch_id=3,
                loss_dyn=1.9,
                q_to_p_ratio=0.4
            )

    def test_convert_transformer_to_b2b_converter_with_amalgamation(self):
        scenario = self.scenario.copy()
        q_to_p_ratio = 0.4
        loss_dyn = 1.9

        for n, (id1, id2, capacity_factor) in enumerate([[2, -2, 1.0],
                                                         [7, -3, np.sqrt(2)]]):
            branch = scenario.branch.loc[id1].copy()
            converter_id, amalgamated = \
                ht.convert_transformer_to_b2b_converter(
                    scenario,
                    branch_id=branch.name,
                    loss_dyn=loss_dyn,
                    q_to_p_ratio=q_to_p_ratio,
                    capacity_factor=capacity_factor,
                    amalgamate=True
                )
            self.assertTrue(converter_id == n + 1)
            self.assertFalse(amalgamated)
            p_capacity = capacity_factor * branch['rating']
            self.verify_acac_converter(scenario, converter_id, branch, q_to_p_ratio,
                                       loss_dyn, p_capacity, amalgamated)

            branch = scenario.branch.loc[id2].copy()
            converter_id, amalgamated = \
                ht.convert_transformer_to_b2b_converter(
                    scenario,
                    branch_id=branch.name,
                    loss_dyn=loss_dyn,
                    q_to_p_ratio=q_to_p_ratio,
                    capacity_factor=capacity_factor,
                    amalgamate=True
                )
            self.assertTrue(converter_id == n + 1)
            self.assertTrue(amalgamated)
            p_capacity += capacity_factor * branch['rating']
            self.verify_acac_converter(scenario, converter_id, branch, q_to_p_ratio,
                                       loss_dyn, p_capacity, amalgamated)

    def verify_acac_converter(self, scenario, converter_id, branch, q_to_p_ratio,
                              loss_dyn, p_capacity, amalgamated):
        self.assertTrue(branch.name not in scenario.branch.index)
        self.assertTrue(converter_id in scenario.converter.index)

        converter = scenario.converter.loc[converter_id]
        p_lim = p_capacity
        q_lim = q_to_p_ratio * p_lim
        if not amalgamated:
            self.assertTrue(
                (converter[['src', 'dst']] == branch[['src', 'dst']]).all()
            )
        else:
            self.assertTrue(
                (converter[['src', 'dst']] == branch[['src', 'dst']].to_numpy()).all() or
                (converter[['src', 'dst']] == branch[['dst', 'src']].to_numpy()).all()
            )
        for cap in [converter['cap_src'], converter['cap_dst']]:
            self.assertTrue(cap.p_max == p_lim)
            self.assertTrue(cap.p_min == -p_lim)
            self.assertTrue(cap.q_max == q_lim)
            self.assertTrue(cap.q_min == -q_lim)
        self.assertTrue((converter[['loss_fwd', 'loss_bwd']] == loss_dyn).all())
        self.assertTrue(converter['loss_fix'] == 0)
        self.assertTrue(converter['annotation'] == branch['annotation'])

    def test_convert_ac_line_to_hvdc_system(self):
        scenario = self.scenario.copy()
        scenario.branch.at[4, 'rating'] = 500.0
        q_to_p_ratio = 0.4
        loss_fwd = 0.85
        loss_bwd = 0.95

        # Convert the branch 2-3
        converter_id_src, converter_id_dst, amalgamated = \
            ht.convert_ac_line_to_hvdc_system(
                scenario,
                branch_id=4,
                loss_fwd=loss_fwd,
                loss_bwd=loss_bwd,
                q_to_p_ratio=q_to_p_ratio,
                amalgamate=True
            )
        self.assertTrue(converter_id_src == 1)
        self.assertTrue(converter_id_dst == 2)
        self.assertTrue(amalgamated == (False, False))
        scenario.verify()
        dc_subgrids = scenario.get_dc_subgrids()
        self.assertTrue(len(dc_subgrids) == 1)
        self.assertTrue(
            set(dc_subgrids[0]) == set(scenario.branch.loc[4, ['src', 'dst']]))

        src, dst = scenario.converter.loc[converter_id_src, ['src', 'dst']]
        ac_base_kv, dc_base_kv = scenario.bus.loc[[src, dst], 'base_kv']

        self.assertTrue(1 < dc_base_kv / ac_base_kv <= np.sqrt(2))
        z_bar = self.scenario.branch.at[4, 'z_bar'].real
        z_bar *= ((ac_base_kv / dc_base_kv) ** 2)
        self.assertTrue(abs(scenario.branch.at[4, 'z_bar'] - z_bar) < 1e-10)

        for id_ in [converter_id_src, converter_id_dst]:
            self.assertTrue(scenario.converter.at[id_, 'loss_fwd'] == loss_fwd)
            self.assertTrue(scenario.converter.at[id_, 'loss_bwd'] == loss_bwd)

        # Convert the branch 3-4 (Amalgamation at bus 3)
        converter_id_src, converter_id_dst, amalgamated = \
            ht.convert_ac_line_to_hvdc_system(
                scenario,
                branch_id=7,
                loss_fwd=loss_fwd,
                loss_bwd=loss_bwd,
                q_to_p_ratio=q_to_p_ratio,
                amalgamate=True
            )
        self.assertTrue(converter_id_src == 2)
        self.assertTrue(converter_id_dst == 3)
        self.assertTrue(amalgamated == (True, False))

        # Convert the branch 4-3 (Amalgamation at bus 3 and 4)
        converter_id_src, converter_id_dst, amalgamated = \
            ht.convert_ac_line_to_hvdc_system(
                scenario,
                branch_id=-3,
                loss_fwd=loss_fwd,
                loss_bwd=loss_bwd,
                q_to_p_ratio=q_to_p_ratio,
                amalgamate=True
            )
        self.assertTrue(converter_id_src == 3)
        self.assertTrue(converter_id_dst == 2)
        self.assertTrue(amalgamated == (True, True))

        # Check the converter (up-)rating
        self.verify_acdc_converter_rating(scenario, q_to_p_ratio)

    def test_convert_ac_line_to_hvdc_system_amalgamation_loop(self):
        scenario = self.scenario.copy()
        scenario.branch.at[2, 'rho_src'] = 1.0
        q_to_p_ratio = 0.4
        loss_fwd = 0.85
        loss_bwd = 0.95

        # Convert the branch 1-4
        converter_id_src, converter_id_dst, amalgamated = \
            ht.convert_ac_line_to_hvdc_system(
                scenario,
                branch_id=2,
                loss_fwd=loss_fwd,
                loss_bwd=loss_bwd,
                q_to_p_ratio=q_to_p_ratio,
                capacity_factor=1.0,
                amalgamate=True
            )
        self.assertTrue(converter_id_src == 1)
        self.assertTrue(converter_id_dst == 2)
        self.assertTrue(amalgamated == (False, False))
        scenario.verify()

        # Convert the branch 5-4 (Amalgamation at bus 4)
        converter_id_src, converter_id_dst, amalgamated = \
            ht.convert_ac_line_to_hvdc_system(
                scenario,
                branch_id=6,
                loss_fwd=loss_fwd,
                loss_bwd=loss_bwd,
                q_to_p_ratio=q_to_p_ratio,
                capacity_factor=1.0,
                amalgamate=True
            )
        self.assertTrue(converter_id_src == 3)
        self.assertTrue(converter_id_dst == 2)
        self.assertTrue(amalgamated == (False, True))
        scenario.verify()

        # Convert the branch 5-1 (Amalgamation only at bus 5 to avoid a loop)
        converter_id_src, converter_id_dst, amalgamated = \
            ht.convert_ac_line_to_hvdc_system(
                scenario,
                branch_id=3,
                loss_fwd=loss_fwd,
                loss_bwd=loss_bwd,
                q_to_p_ratio=q_to_p_ratio,
                capacity_factor=1.0,
                amalgamate=True
            )
        self.assertTrue(converter_id_src == 3)
        self.assertTrue(converter_id_dst == 4)
        self.assertTrue(amalgamated == (True, False))

        # Bus 5 is now an individual AC subgrid without a reference
        with self.assertRaises(ValueError):
            scenario.verify()
        scenario.ensure_reference()
        scenario.verify()

        # We have one 4-terminal HVDC system
        dc_subgrids = scenario.get_dc_subgrids()
        self.assertTrue(len(dc_subgrids) == 1)
        self.assertTrue(len(dc_subgrids[0]) == 4)

        # Check the converter (up-)rating
        self.verify_acdc_converter_rating(scenario, q_to_p_ratio)

    def verify_acdc_converter_rating(self, scenario, q_to_p_ratio):
        for converter_id, converter in scenario.converter.iterrows():
            dc_bus_id = converter['dst']
            dc_branches = scenario.branch.loc[
                (scenario.branch[['src', 'dst']] == dc_bus_id).any(axis='columns')
            ]
            p_lim = dc_branches['rating'].sum()
            q_lim = q_to_p_ratio * p_lim

            cap_src = converter['cap_src']
            self.assertTrue(cap_src.p_max == p_lim)
            self.assertTrue(cap_src.p_min == -p_lim)
            self.assertTrue(cap_src.q_max == q_lim)
            self.assertTrue(cap_src.q_min == -q_lim)

            cap_dst = converter['cap_dst']
            self.assertTrue(cap_dst.p_max == p_lim)
            self.assertTrue(cap_dst.p_min == -p_lim)
            self.assertTrue(cap_dst.q_max == 0)
            self.assertTrue(cap_dst.q_min == 0)

    def test_mst_branch_functions(self):
        # System with acyclic subgrids
        scenario = TEST_SYSTEM.copy()
        branch_weights = get_series_resistance_weights(scenario)
        branches = get_mst_branches(scenario, branch_weights)
        self.assertTrue(branches.equals(scenario.branch.index))

        branches = get_branches_outside_mst(scenario, branch_weights)
        self.assertTrue(branches.empty)

        # Meshed system with two cycles
        scenario = self.scenario.copy()
        branch_weights = get_series_resistance_weights(scenario)
        branches = get_mst_branches(scenario, branch_weights)
        self.assertTrue(set(branches) == set(scenario.branch.index) - set([1, 6]))

        branches = get_branches_outside_mst(scenario, branch_weights)
        self.assertTrue(set(branches) == set([1, 6]))

    def test_get_islanding_branches(self):
        scenario = TEST_SYSTEM.copy()
        self.assertTrue(len(scenario.get_islands()) == 3)

        # Only the branches in the subgrid [9, 10] are "islanding branches"
        branches = ht.get_islanding_branches(scenario)
        self.assertTrue(set(branches) == set([5, -1]))

        # Remove the subgrids [9, 10] and [11] => fully meshed system
        scenario.remove_buses([9, 10, 11])
        self.assertTrue(len(scenario.get_islands()) == 1)
        branches = ht.get_islanding_branches(scenario)
        self.assertTrue(branches.empty)

        # Remove the DC subgrid => radial AC system with a converter btw. 1-2
        scenario.remove_buses([6, 7, 8])
        self.assertTrue(len(scenario.get_islands()) == 1)
        self.assertTrue(scenario.has_acyclic_subgrids())
        branches = ht.get_islanding_branches(scenario)
        self.assertTrue(set(branches) == set(scenario.branch.index) - set([1]))

        # Remove the converter => radial AC system
        scenario.converter.drop(scenario.converter.index, inplace=True)
        branches = ht.get_islanding_branches(scenario)
        self.assertTrue(branches.equals(scenario.branch.index))
