import unittest

import hynet as ht
from hynet.solver.ipopt import QCQPSolver
from hynet.test.system import TEST_SYSTEM

REFERENCE_SUMMARY = """
+------------------------------------------------------------------------------+
| OPTIMAL POWER FLOW                                     hynet ~ version 1.2.2 |
|                                                                              |
|> Data Source ---------------------------------------------------------------<|
|                                                                              |
| Grid:       Artificial System for Testing Purposes (test_system.db)          |
| Scenario:   default @ 1d 01:15:36 (id=0)                                     |
|                                                                              |
|> Grid Information ----------------------------------------------------------<|
|                                                                              |
| Topology: hyb. arch.|      2 AC subgrids|     2 DC subgrids|    2 islands    |
| Buses:           11 |      7 AC buses   |     4 DC buses   |    2 with shunt |
| Branches:        10 |      5 AC lines   |     3 DC lines   |    2 transform. |
| Converters:       5 |      3 AC/DC      |     1 AC/AC      |    1 DC/DC      |
| Injectors:        6 |      1 convent.   |     1 disp. load |    0 compensat. |
|                     |      3 renewable  |     1 prosumer   |                 |
|                                                                              |
| Injection:     1490.0 MW /  1020.0 Mvar | Min.:      0.0 MW /   -1020.0 Mvar |
| Total load:    1041.3 MW /   342.0 Mvar | Loading:  69.89% of P-capacity     |
|                                                                              |
|> Results -------------------------------------------------------------------<|
|                                                                              |
| Injection:     1054.1 MW /   355.3 Mvar | Inj. cost:           19.652 k$/h   |
| Total loss:      12.8 MW / 1.23% P-load | Shunt loss:           3.6 MW       |
| Branch loss:      4.5 MW                | Shunt Q-sum:         11.9 Mvar     |
| Conv. loss:       4.7 MW                | Conv. Q-sum:          7.1 Mvar     |
| Loss price:       0.250 $/MWh           | Loss penalty:         0.003 k$/h   |
|                                        -+----+-                              |
| Voltage mag.:     1.018 to     1.100 p.u.    | Average branch utilization:   |
| Voltage angle:   -0.85  to     5.44 deg.     |  AC lines:      45.66%        |
| P-bal. dual:     -0.00  to    58.06 $/MWh    |  DC lines:      36.74%        |
| Q-bal. dual:      0.00  to   716.53 $/Mvarh  |  Transformers:  62.83%        |
|                                                                              |
|> Solution Process ----------------------------------------------------------<|
|                                                                              |
| Solver:             IPOPT (QCQP) / tol=1e-06                                 |
| Status:             solved                                                   |
| Time:               0.07 sec. in the solver / 0.23 sec. in total             |
| Power balance:      4.270e-07 (1.708e-06) mean (max.) absolute error in MVA  |
| Converters:         4.744e-06 (7.694e-06) mean (max.) loss error in MW       |
+------------------------------------------------------------------------------+
"""

REFERENCE_DETAILS = """
+------------------------------------------------------------------------------+
| BUS RESULT                                                                   |
+-------+---------------+-------------------+--------------+-------------------+
|       |    Voltage    |       Load        |    Shunt     |  Dual Variables   |
|       +-------+-------+---------+---------+------+-------+---------+---------+
|       |  Mag. | Phase |    P    |    Q    | Loss | Q-Inj.|  P-bal. |  Q-bal. |
|  ID   |  (pu) | (deg) |   (MW)  |  (Mvar) | (MW) | (Mvar)| ($/MWh) |($/Mvarh)|
+-------+-------+-------+---------+---------+------+-------+---------+---------+
|     1 |  1.095|  4.060|      -  |      -  |  -   |  5.992|   11.242|   10.194|
|     2 |  1.089|  0.060|   300.00|   100.00| 3.558|  5.930|   30.236|    3.470|
|R    3 |* 1.100|  0.000|   300.00|   100.00|  -   |   -   |   30.000|    1.000|
|     4 |  1.018|  3.240|   400.00|   129.00|  -   |   -   |   30.687|  716.525|
|     5 |* 1.100|  5.441|      -  |      -  |  -   |   -   |   10.000|    1.000|
|R    6=|  1.097|   -   |      -  |      -  |  -   |   -   |   30.356|    0.000|
|     7=|  1.096|   -   |      -  |      -  |  -   |   -   |   30.377|    0.000|
|     8=|* 1.100|   -   |      -  |      -  |  -   |   -   |   30.071|    0.000|
|R    9 |* 1.100|  0.000|    38.00|    13.00|  -   |   -   |   58.060|    1.018|
|    10 |  1.049| -0.847|      -  |      -  |  -   |   -   |   58.000|    1.000|
|    11=|  1.028|   -   |     3.30|      -  |  -   |   -   |   -0.000|    0.000|
+-------+-------+-------+---------+---------+------+-------+---------+---------+

+------------------------------------------------------------------------------+
| BRANCH RESULT                                                                |
+-------+-------------+-------------------+-----------------+--------+---------+
|       |  Terminals  |    Power Flow     |     Voltage     | Losses | Reconst.|
|       +------+------+----------+--------+--------+--------+--------+---------+
|       | Src. | Dst. | |S_src|  | PF_src |  Drop  |Angle D.|  Dyn.  |  Kappa  |
|  ID   | Bus  | Bus  |  (MVA)   |        |  (%)   | (deg)  |  (MW)  |         |
+-------+------+------+----------+--------+--------+--------+--------+---------+
|     1 |    1 |    2 |    296.24|   1.000|  -0.516|* -4.000|   2.058|     -   |
|     2T|    1 |    4 |    114.48|   0.814|* -7.000|* -0.820|   0.370|     -   |
|     3T|    5 |    1 |    461.08|   0.994|  -0.481|  -1.381|   1.125|     -   |
|     4 |    2 |    3 |    111.50|   0.005|   1.005|  -0.060|   0.111|     -   |
|     7 |    6=|    7=|     14.01|   1.000|  -0.035|    -   |   0.005|     -   |
|     6 |    8=|    7=|*   137.50|   1.000|  -0.337|    -   |   0.464|     -   |
|     5 |   10 |    9 |     20.12|   0.945|*  4.814|   0.847|   0.009|     -   |
|    -1 |    9 |   10 |     20.08|  -0.946|  -4.593|  -0.847|   0.009|     -   |
|    -2 |    1 |    4 |    114.48|   0.814|  -7.000|  -0.820|   0.370|     -   |
|    -3 |    6=|    7=|     14.01|   1.000|  -0.035|    -   |   0.005|     -   |
+-------+------+------+----------+--------+--------+--------+--------+---------+

+------------------------------------------------------------------------------+
| CONVERTER RESULT                                                             |
+-------+-------------+---------------------+-----------------+----------------+
|       |  Terminals  |     Power Flow      |Reactive Pwr Inj.|     Losses     |
|       +------+------+----------+----------+--------+--------+--------+-------+
|       | Src. | Dst. |  P_src   |  P_dst   | Q_src  | Q_dst  |  Dyn.  |  Fix  |
|  ID   | Bus  | Bus  |   (MW)   |   (MW)   | (Mvar) | (Mvar) |  (MW)  |  (MW) |
+-------+------+------+----------+----------+--------+--------+--------+-------+
|     1 |    3 |    6=|     28.31|    -28.02|    4.08|     -  |   0.283|  0.750|
|     2 |    7=|    4 |    216.53|   -214.37|     -  |    2.00|   2.165|   -   |
|     3 |    5 |    8=|    140.40|*  -139.00|    1.00|     -  |   1.404|   -   |
|     4 |    1 |    2 |*    10.00|     -9.90|   -0.00|   -0.00|   0.100|   -   |
|     5 |    8=|    7=|      1.50|     -1.49|     -  |     -  |   0.015|   -   |
+-------+------+------+----------+----------+--------+--------+--------+-------+

+------------------------------------------------------------------------------+
| INJECTOR RESULT                                                              |
+-------+------+--------------------------+-----------------+------------------+
|       | Term.|        Injection         |      Cost       |       Type       |
|       +------+----------+--------+------+--------+--------+                  |
|       |      |    P     |   Q    |  PF  | for P  | for Q  |                  |
|  ID   | Bus  |   (MW)   | (Mvar) |      | (k$/h) | (k$/h) |                  |
+-------+------+----------+--------+------+--------+--------+------------------+
|     1 |    1 |     35.23|   88.07| 0.371|   0.493|   0.088| CONVENTIONAL     |
|     2 |    3 |    328.65|  206.31| 0.847|   9.860|   0.206| RENEWABLE        |
|     3 |    5 |*   598.90|   47.73| 0.997|   5.989|   0.048| PROSUMER         |
|     4 |    7=|*    50.00|     -  |  -   |   0.750|    -   | LOAD             |
|     5 |   10 |     38.02|   13.19| 0.945|   2.205|   0.013| RENEWABLE (WIND) |
|     6 |   11=|      3.30|     -  |  -   |    -   |    -   | RENEWABLE (PV)   |
+-------+------+----------+--------+------+--------+--------+------------------+
"""


class OptimalPowerFlowFormattingTest(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test(self):
        result = ht.calc_opf(TEST_SYSTEM, solver=QCQPSolver())

        # Yes, we know, this unit test is kind of filthy :) Due to the
        # numerical differences of the result for different installations,
        # only a small fraction of the formatted output is actually verified.
        # However, we did not want to add the data of an entire OPF result
        # just for the purpose of this test. Still, this UT can be very
        # handy for regression tests during performance optimization if the
        # comparison of the entire output is enabled below.

        self.compare_output(str(result), REFERENCE_SUMMARY,
                            skip=['hynet ~ version',
                                  'Injection:',
                                  'Total loss:',
                                  'Branch loss:',
                                  'Conv. loss:',
                                  'Voltage mag.:',
                                  'Voltage angle:',
                                  'P-bal. dual:',
                                  'Q-bal. dual:',
                                  'Time:',
                                  'Power balance:',
                                  'loss error in MW',
                                  'Voltage recovery:'])

        # self.compare_output(result.details, REFERENCE_DETAILS,
        #                     skip=['|R    6=|',
        #                           '|     7=|',
        #                           '|     8=|',
        #                           '|    11=|'])

    def compare_output(self, result, reference, skip=None):
        if skip is None:
            skip = []
        res = result.strip().splitlines()
        ref = reference.strip().splitlines()

        self.assertTrue(len(res) == len(ref), "Number of lines does not match.")

        for line_res, line_ref in zip(res, ref):
            if any([s in line_ref for s in skip]):
                continue
            self.assertTrue(line_res == line_ref,
                            "Lines does not match:\nOUT: " + line_res +
                            "\nREF: " + line_ref)
