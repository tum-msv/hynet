import unittest

import numpy as np

import hynet as ht
from hynet.data.connection import DBTransaction
from hynet.data.structure import (DBInfo,
                                  DBBus,
                                  DBBranch,
                                  DBConverter,
                                  DBCapabilityRegion,
                                  DBShunt,
                                  DBInjector,
                                  DBSamplePoint,
                                  DBScenario,
                                  DBScenarioLoad,
                                  DBScenarioInjector,
                                  DBScenarioInactivity)
from hynet.types_ import DBInfoKey
from tests.test_utilities import remove_temporary_file

TEST_DB_PATH = "_hynet_test_import.db"
TEST_MAT_PATH = "tests/data/"
TEST_MAT_V6 = "test_case_v6.mat"
TEST_MAT_V73 = "test_case_v73.mat"
TEST_MAT_OPT = "test_case_optional_data.mat"


class ImportTest(unittest.TestCase):

    def setUp(self):
        remove_temporary_file(TEST_DB_PATH)

    def tearDown(self):
        remove_temporary_file(TEST_DB_PATH)

    def test_mat_v6_import(self):
        ht.import_matpower_test_case(input_file=(TEST_MAT_PATH + TEST_MAT_V6),
                                     output_file=TEST_DB_PATH,
                                     grid_name="Import test grid",
                                     description="Import test description")
        self.assert_database()
        self.assert_scenario(ensure_reference=True)

    def test_mat_v73_import(self):
        ht.import_matpower_test_case(input_file=(TEST_MAT_PATH + TEST_MAT_V73),
                                     output_file=TEST_DB_PATH,
                                     grid_name="Import test grid",
                                     description="Import test description")
        self.assert_database()
        self.assert_scenario(ensure_reference=True)

    def test_mat_optional_data(self):
        ht.import_matpower_test_case(input_file=(TEST_MAT_PATH + TEST_MAT_OPT),
                                     output_file=TEST_DB_PATH,
                                     grid_name="Import test grid",
                                     description="Import test description")
        self.assert_scenario(angle_limits=False, ramping_limits=False)

    def assert_database(self):
        database = ht.connect(TEST_DB_PATH)
        with DBTransaction(database) as transaction:
            info = {x.key: x.value for x in transaction.query(DBInfo).all()}
            buses = transaction.query(DBBus)
            branches = transaction.query(DBBranch)
            converters = transaction.query(DBConverter)
            cap_regions = transaction.query(DBCapabilityRegion)
            shunts = transaction.query(DBShunt)
            injectors = transaction.query(DBInjector)
            sample_points = transaction.query(DBSamplePoint)
            scenarios = transaction.query(DBScenario)
            scenario_loads = transaction.query(DBScenarioLoad)
            scenario_injectors = transaction.query(DBScenarioInjector)
            scenario_inactivities = transaction.query(DBScenarioInactivity)

            self.assertEqual(info[DBInfoKey('version')],
                             '1.0',
                             "Invalid version number.")
            self.assertEqual(info[DBInfoKey('grid_name')],
                             'Import test grid',
                             "Invalid grid name.")
            self.assertEqual(info[DBInfoKey('description')],
                             'Import test description',
                             "Invalid description.")

            self.assertEqual(buses.count(), 8, "Invalid number of buses.")
            self.assertEqual(branches.count(), 6,
                             "Invalid number of branches.")
            self.assertEqual(converters.count(), 3,
                             "Invalid number of converters.")
            self.assertEqual(cap_regions.count(),
                             converters.count()*2 + injectors.count(),
                             "Invalid number of capability regions.")
            self.assertEqual(shunts.count(), 0, "Invalid number of shunts.")
            self.assertEqual(injectors.count(), 4,
                             "Invalid number of injectors.")
            self.assertEqual(sample_points.count(), 2*injectors.count(),
                             "Invalid number of sample points.")
            self.assertEqual(scenarios.count(), 97,
                             "Invalid number of scenarios.")
            self.assertEqual(scenario_loads.count(), 3*scenarios.count(),
                             "Invalid number of scenario loads.")
            self.assertEqual(scenario_injectors.count(), 0,
                             "Invalid number of scenario injectors.")
            self.assertEqual(scenario_inactivities.count(), scenarios.count(),
                             "Invalid number of scenario inactivities.")

            # Bus parameter tests
            bus = buses.first()
            self.assertEqual(bus.base_kv, 230, "Invalid kV-base of bus 1.")
            self.assertEqual(bus.v_min, 0.9, "Invalid v_min of bus 1.")
            self.assertEqual(bus.v_max, 1.1, "Invalid v_max of bus 1.")

            # Branch parameter tests
            branch = branches.first()
            self.assertEqual(branch.src_id, 1,
                             "Invalid source bus ID of branch 1.")
            self.assertEqual(branch.dst_id, 2,
                             "Invalid destination bus ID of branch 1.")
            self.assertEqual(branch.rating, 400,
                             "Invalid rating of branch 1.")

            # Converter parameter tests
            converter = converters.first()
            self.assertEqual(converter.src_id, 3,
                             "Invalid source bus ID of converter 1.")
            self.assertEqual(converter.loss_fwd, 1.0,
                             "Invalid forward dynamic losses of converter 1.")
            self.assertEqual(converter.loss_bwd, 2.0,
                             "Invalid backward dynamic losses of converter 1.")
            self.assertEqual(converter.loss_fix, 5.0,
                             "Invalid static losses of converter 1.")

            # Capability region parameter tests
            cap_region = cap_regions.first()
            self.assertEqual(cap_region.p_min, -500,
                             "Invalid p_min of capability region 1.")
            self.assertEqual(cap_region.q_min, 0,
                             "Invalid q_min of capability region 1.")

            # Injector parameter tests
            injector = injectors.first()
            self.assertEqual(injector.cost_start, 100.0,
                             "Invalid startup costs for injector 1.")
            self.assertEqual(injector.ramp_up, 105.0,
                             "Invalid up-ramping limit for injector 1.")

    def assert_scenario(self, angle_limits=True, ramping_limits=True,
                        ensure_reference=False):
        database = ht.connect(TEST_DB_PATH)
        self.assertFalse(database.empty, "The imported test database is empty.")
        scenario = ht.load_scenario(database)
        if ensure_reference:
            scenario.ensure_reference()
        scenario.verify()
        self.assertTrue(np.all(
            ~np.isnan(scenario.injector['ramp_up'].to_numpy()) == ramping_limits))
        self.assertTrue(np.all(
            ~np.isnan(scenario.injector['ramp_down'].to_numpy()) == ramping_limits))
        self.assertTrue(np.all(
            ~np.isnan(scenario.branch['angle_min'].to_numpy()) == angle_limits))
        self.assertTrue(np.all(
            ~np.isnan(scenario.branch['angle_max'].to_numpy()) == angle_limits))
