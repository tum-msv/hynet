import logging
import unittest

import numpy as np

import hynet as ht
from hynet.test.system import TEST_SYSTEM

from hynet.reduction.large_scale.features import (add_bus_features,
                                                  add_converter_features,
                                                  add_branch_features,
                                                  add_congestion_features)
from hynet.reduction.large_scale.topology import (reduce_single_buses,
                                                  reduce_islands)
from hynet.reduction.large_scale.coupling import (reduce_by_coupling,
                                                  get_critical_injector_features)
from hynet.reduction.large_scale.market import (identify_price_clusters,
                                                reduce_by_market)
from hynet.reduction.large_scale.combination import reduce_system
from hynet.reduction.large_scale.subgrid import (restore_aggregation_info,
                                                 RETAIN_LINE_CHARGING)
from hynet.reduction.large_scale.utilities import (add_parallel_branch_info,
                                                   add_adjacent_bus_info)

_log = logging.getLogger(__name__)


class ReductionTest(unittest.TestCase):

    def setUp(self):
        self._scenario = TEST_SYSTEM.copy()

    def tearDown(self):
        pass

    def verify_features(self, buses=None, branches=None):
        buses = set(buses) if buses is not None else set()
        branches = set(branches) if branches is not None else set()

        self.assertTrue('feature' in self._scenario.bus.columns)
        feature_buses = \
            self._scenario.bus.index[self._scenario.bus['feature']]
        self.assertTrue(set(feature_buses) == buses)

        self.assertTrue('feature' in self._scenario.branch.columns)
        feature_branches = \
            self._scenario.branch.index[self._scenario.branch['feature']]
        self.assertTrue(set(feature_branches) == branches)

    def test_bus_features(self):
        add_bus_features(self._scenario)
        self.verify_features(buses=[1, 3, 6, 9])

    def test_converter_features(self):
        add_converter_features(self._scenario)
        self.verify_features(buses=[1, 2, 3, 4, 5, 6, 7, 8])

    def test_branch_features(self):
        self._scenario.branch.loc[6, 'length'] = 50
        add_branch_features(self._scenario, length_threshold=50)
        self.verify_features(branches=[2, 3, 6])

    def test_congestion_features(self):
        result = ht.calc_opf(self._scenario)
        add_congestion_features(self._scenario,
                                result,
                                loading_threshold=0.8,
                                dv_threshold=1.0)
        # Loading + angle_min + angle_max&drop_min + drop_max
        self.verify_features(branches=[3, 6] + [1] + [2] + [5])

    def test_parallel_branch_info(self):
        add_parallel_branch_info(self._scenario)
        idx_parallel = \
            self._scenario.branch.index[self._scenario.branch['parallel']]
        self.assertTrue(set(idx_parallel) == set([2, -2, 5, -1, 7, -3]))
        self.assertTrue(
            set(self._scenario.branch.loc[idx_parallel,
                                          'parallel_main_id']) == set([2, 5, 7]))

    def test_adjacent_bus_info(self):
        add_adjacent_bus_info(self._scenario)
        num_adjacent = [3, 2] + [1]*4 + [2] + [1]*3 + [0]
        self.assertTrue(all(self._scenario.bus['num_adjacent'] == num_adjacent))

    def test_reduce_single_buses(self):
        iterations = 0
        while reduce_single_buses(self._scenario) > 0:
            iterations += 1
        num_subgrids = len(TEST_SYSTEM.get_ac_subgrids()) \
                     + len(TEST_SYSTEM.get_dc_subgrids())
        self._scenario.verify()
        self.assertTrue(iterations == 2)
        self.assertTrue(self._scenario.num_branches == 0)
        self.assertTrue(self._scenario.num_buses == num_subgrids)
        self.assertTrue(self._scenario.num_converters == 3)
        self.assertTrue(self._scenario.num_injectors == TEST_SYSTEM.num_injectors)
        self.assertTrue(set(self._scenario.bus.index) == set([2, 7, 10, 11]))
        self.assertTrue(
            set(self._scenario.bus.loc[2, 'aggregation']) == set([1, 3, 4, 5]))
        self.assertTrue(
            set(self._scenario.bus.loc[7, 'aggregation']) == set([6, 8]))
        self.assertTrue(
            set(self._scenario.bus.loc[10, 'aggregation']) == set([9]))
        self.assertTrue(
            set(self._scenario.bus.loc[11, 'aggregation']) == set())
        self.assertTrue((self._scenario.injector.loc[[1, 2, 3], 'bus'] == 2).all())
        self.assertTrue((self._scenario.injector.loc[[4, 5, 6], 'bus'] ==
                         TEST_SYSTEM.injector.loc[[4, 5, 6], 'bus']).all())
        self.assertTrue(self._scenario.bus['load'].sum() ==
                        TEST_SYSTEM.bus['load'].sum())
        if RETAIN_LINE_CHARGING:
            self.assertTrue(self._scenario.bus['y_tld'].sum() ==
                            TEST_SYSTEM.bus['y_tld'].sum() +
                            TEST_SYSTEM.branch[['y_src', 'y_dst']].sum().sum())
        else:
            self.assertTrue(self._scenario.bus['y_tld'].sum() ==
                            TEST_SYSTEM.bus['y_tld'].sum())

    def test_reduce_single_buses_with_features(self):
        self._scenario.bus['feature'] = True
        iterations = 0
        while reduce_single_buses(self._scenario) > 0:
            iterations += 1
        self.assertTrue(iterations == 0)
        self._scenario.bus['feature'] = False
        self._scenario.branch['feature'] = True
        while reduce_single_buses(self._scenario) > 0:
            iterations += 1
        self.assertTrue(iterations == 0)
        self._scenario.bus.drop('feature', axis='columns', inplace=True)
        self._scenario.branch.drop('feature', axis='columns', inplace=True)
        self.assertTrue(self._scenario == TEST_SYSTEM)

    def test_reduce_islands(self):
        reduce_islands(self._scenario, max_island_size=0)
        self.assertTrue(self._scenario == TEST_SYSTEM)
        with self.assertRaises(ValueError):
            reduce_islands(self._scenario, max_island_size=5)
        self._scenario.remove_buses([6, 7, 8, 9, 10, 11])
        self._scenario.converter = self._scenario.converter.iloc[0:0]
        reduce_islands(self._scenario, max_island_size=5)
        self.assertTrue(self._scenario.num_branches == 0)
        self.assertTrue(self._scenario.num_buses == 1)
        self.assertTrue((self._scenario.injector['bus'] == 1).all())
        self.assertTrue(
            set(self._scenario.bus.loc[1, 'aggregation']) == set([2, 3, 4, 5]))

    def test_reduce_islands_with_max_size(self):
        self._scenario.remove_buses([6, 7, 8, 9, 10, 11])
        self._scenario.converter = self._scenario.converter.iloc[0:0]
        reduce_islands(self._scenario, max_island_size=1)
        self.assertTrue(self._scenario.num_branches == 1)
        self.assertTrue(self._scenario.num_buses == 2)
        self.assertTrue(
            set(self._scenario.bus.loc[1, 'aggregation']) == set([4, 5]))
        self.assertTrue(
            set(self._scenario.bus.loc[2, 'aggregation']) == set([3]))

    def test_reduce_islands_with_features(self):
        self._scenario.remove_buses([6, 7, 8, 9, 10, 11])
        self._scenario.converter = self._scenario.converter.iloc[0:0]
        reference = self._scenario.copy()
        self._scenario.bus['feature'] = True
        self.assertTrue(reduce_islands(self._scenario, max_island_size=5) == 0)
        self._scenario.bus['feature'] = False
        self._scenario.branch['feature'] = True
        self.assertTrue(reduce_islands(self._scenario, max_island_size=5) == 0)
        self._scenario.bus.drop('feature', axis='columns', inplace=True)
        self._scenario.branch.drop('feature', axis='columns', inplace=True)
        self.assertTrue(self._scenario == reference)

    def test_reduce_by_coupling(self):
        self.assertTrue(reduce_by_coupling(self._scenario,
                                           rel_impedance_thres=0.2) == 2)
        self.assertTrue(self._scenario.num_buses == 9)
        self.assertTrue(self._scenario.num_branches == 6)
        self.assertTrue(all(~self._scenario.branch.index.isin([5, -1, 7, -3])))

    def test_reduce_by_coupling_with_features(self):
        self._scenario.branch['feature'] = False
        self._scenario.branch.at[5, 'feature'] = True
        self.assertTrue(reduce_by_coupling(self._scenario,
                                           rel_impedance_thres=0.2) == 1)
        self.assertTrue(self._scenario.num_buses == 10)
        self.assertTrue(self._scenario.num_branches == 8)
        self.assertTrue(all(~self._scenario.branch.index.isin([7, -3])))

    def test_critical_injector_features(self):
        reduction = self._scenario.copy()
        self.assertTrue(reduce_by_coupling(reduction,
                                           rel_impedance_thres=0.2) == 2)
        opf_reference = ht.calc_opf(self._scenario)
        opf_reduction = ht.calc_opf(reduction)
        critical_buses = get_critical_injector_features(opf_reference,
                                                        opf_reduction,
                                                        feature_depth=2,
                                                        critical_mw_diff=1e-2)
        self.assertTrue(set(critical_buses) == set([10, 9]))
        critical_buses = get_critical_injector_features(opf_reference,
                                                        opf_reduction,
                                                        feature_depth=2,
                                                        critical_mw_diff=1e-3)
        self.assertTrue(set(critical_buses) == set([3, 10, 2, 9, 1]))

    def test_identify_price_clusters_single(self):
        result = ht.calc_opf(self._scenario)

        clusters = identify_price_clusters(result, max_price_diff=0.1)
        self.assertTrue(len(clusters.index) == 1)
        self.assertTrue(clusters.at[7, 'cluster'] == set([7, 6]))
        self.assertTrue(clusters.at[7, 'price_ref'] == result.bus.at[7, 'dv_bal_p'])

        clusters = identify_price_clusters(result, max_price_diff=0.4)
        self.assertTrue(len(clusters.index) == 2)
        self.assertTrue(clusters.at[7, 'cluster'] == set([7, 6, 8]))
        self.assertTrue(clusters.at[2, 'cluster'] == set([2, 3]))

        clusters = identify_price_clusters(result, max_price_diff=1.5)
        self.assertTrue(len(clusters.index) == 3)
        self.assertTrue(clusters.at[7, 'cluster'] == set([7, 6, 8]))
        self.assertTrue(clusters.at[2, 'cluster'] == set([2, 3]))
        self.assertTrue(clusters.at[1, 'cluster'] == set([1, 5]))

    def test_identify_price_clusters_single_with_features(self):
        result = ht.calc_opf(self._scenario)
        self._scenario.bus['feature'] = False
        self._scenario.bus.loc[[1, 2, 6], 'feature'] = True

        clusters = identify_price_clusters(result, max_price_diff=1.5)
        self.assertTrue(len(clusters.index) == 1)
        self.assertTrue(clusters.at[7, 'cluster'] == set([7, 8]))

    def test_identify_price_clusters_overlapping(self):
        self._scenario.branch.at[5, 'dst'] = 3
        self._scenario.branch.at[-1, 'dst'] = 3
        self._scenario.bus.loc[[9, 10], 'base_kv'] = 380.0
        self._scenario.bus.loc[9, 'ref'] = False
        self._scenario.branch[['angle_min', 'angle_max', 'drop_min', 'drop_max']] = np.nan
        self._scenario.bus.loc[10, 'v_min'] = 0.9
        self._scenario.verify()
        self._scenario.bus['feature'] = False
        self._scenario.bus.at[7, 'feature'] = True
        result = ht.calc_opf(self._scenario)
        # 1) Two clusters (2 of 3 combined)
        clusters = identify_price_clusters(result, max_price_diff=0.5)
        self.assertTrue(len(clusters.index) == 2)
        self.assertTrue(clusters.at[1, 'cluster'] == set([1, 4, 5]))
        center = 2 if 2 in clusters.index else 3
        self.assertTrue(clusters.at[center, 'cluster'] == set([3, 2, 9, 10]))
        self.assertTrue((clusters.loc[[1, center], 'price_ref'] ==
                         result.bus.loc[[1, center], 'dv_bal_p']).all())
        # 2) One combined cluster
        clusters = identify_price_clusters(result, max_price_diff=1.0)
        self.assertTrue(len(clusters.index) == 1)
        self.assertTrue(clusters.at[1, 'cluster'] == set([1, 2, 3, 4, 5, 9, 10]))

    def test_reduce_by_market(self):
        result = ht.calc_opf(self._scenario)
        self.assertFalse(self._scenario.injector.at[3, 'bus'] == 1)
        self._scenario.bus['feature'] = False
        self._scenario.bus.loc[[2, 7], 'feature'] = True
        self.assertTrue(reduce_by_market(self._scenario,
                                         result,
                                         max_price_diff=1.5) == 1)
        self.assertTrue(5 not in self._scenario.bus.index)
        self.assertTrue(self._scenario.injector.at[3, 'bus'] == 1)

    def test_reduce_by_market_with_load_aggregation(self):
        self._scenario.bus['feature'] = False
        self._scenario.bus.loc[[5, 7], 'feature'] = True
        result = ht.calc_opf(self._scenario)
        self.assertTrue(reduce_by_market(self._scenario,
                                         result,
                                         max_price_diff=30) == 3)
        center = 1 if 1 in self._scenario.bus.index else 2
        self.assertFalse(set(self._scenario.bus.index)
                         .intersection(set([1, 2, 3, 4]) - set([center])))
        self.assertTrue(self._scenario.injector.at[1, 'bus'] == center)
        self.assertTrue(self._scenario.injector.at[2, 'bus'] == center)
        self.assertTrue(self._scenario.injector.at[3, 'bus'] == 5)
        self.assertTrue(self._scenario.bus.at[center, 'load'] ==
                        TEST_SYSTEM.bus.loc[1:4, 'load'].sum())

    def test_reduce_system_coupling(self):
        self._scenario.bus['feature'] = False
        self._scenario.branch['feature'] = False
        evaluation, bus_id_map = \
            reduce_system(self._scenario,
                          solver=None,
                          max_island_size=-1,
                          rel_impedance_thres=0.2,
                          feature_depth=0,
                          critical_mw_diff=None,
                          max_price_diff=0.0,
                          show_evaluation=False,
                          return_bus_id_map=True,
                          preserve_aggregation=True)
        self.assertTrue(len(evaluation.index) == 2)
        self.assertTrue(evaluation.at['', 'bus_reduction'] == 0)
        self.assertTrue(evaluation.at['Coupling', 'bus_reduction'] == 2/11)
        self.assertTrue(evaluation.at['Coupling', 'branch_reduction'] == 4/10)
        self.assertTrue(evaluation.at['Coupling', 'error_disp'] <= 1e-4)
        self.assertTrue(evaluation.at['Coupling', 'error_flow'] <= 1e-5)
        equivalence = [1, 2, 3, 4, 5, 6, 8, 10, 11]
        self.assertTrue(all(bus_id_map.loc[equivalence] == equivalence))
        self.assertTrue(bus_id_map.at[7] == 6)
        self.assertTrue(bus_id_map.at[9] == 10)
        self.assertTrue(self._scenario.num_buses == 9)
        self.assertTrue((self._scenario.bus.loc[[1, 2, 3, 4, 5, 8, 11],
                                                'annotation'] == '').all())
        self.assertTrue(self._scenario.bus.at[6,
                                              'annotation'] == 'aggregates [7]')
        self.assertTrue(self._scenario.bus.at[10,
                                              'annotation'] == 'aggregates [9]')
        with self.assertRaises(ValueError):
            restore_aggregation_info(self._scenario)
        aggregation = self._scenario.bus.pop('aggregation')
        restore_aggregation_info(self._scenario)
        self.assertTrue(self._scenario.bus['aggregation'].equals(aggregation))

    def test_reduce_system_topology(self):
        self._scenario.bus['feature'] = False
        self._scenario.branch['feature'] = False
        evaluation = \
            reduce_system(self._scenario,
                          solver=None,
                          max_island_size=0,
                          rel_impedance_thres=0,
                          feature_depth=0,
                          critical_mw_diff=None,
                          max_price_diff=0.0,
                          show_evaluation=False,
                          return_bus_id_map=False,
                          preserve_aggregation=True)
        self.assertTrue(len(evaluation.index) == 2)
        self.assertTrue(evaluation.at['Topology', 'bus_reduction'] == 7/11)
        self.assertTrue(evaluation.at['Topology', 'branch_reduction'] == 1)
        self.assertTrue(np.isnan(evaluation.at['Topology', 'error_flow']))
        aggregation = self._scenario.bus.pop('aggregation')
        restore_aggregation_info(self._scenario)
        self.assertTrue(self._scenario.bus['aggregation'].equals(aggregation))
