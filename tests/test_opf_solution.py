import logging
import unittest

import hynet as ht
import hynet.config as config
from hynet.solver.ipopt import QCQPSolver
from hynet.test.system import TEST_SYSTEM

_log = logging.getLogger(__name__)


class OptimalPowerFlowSolutionTest(unittest.TestCase):

    def setUp(self):
        self.parallelize = config.GENERAL['parallelize']

    def tearDown(self):
        config.GENERAL['parallelize'] = self.parallelize

    def test_with_parallel_processing(self):
        config.GENERAL['parallelize'] = True
        self.assertTrue(ht.test_installation(),
                        "The reference OPF was not solved properly.")

    def test_without_parallel_processing(self):
        config.GENERAL['parallelize'] = False
        self.assertTrue(ht.test_installation(),
                        "The reference OPF was not solved properly.")


class OptimalPowerFlowEvaluationTest(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_branch_utilization(self):
        result = ht.calc_opf(TEST_SYSTEM.copy(), solver=QCQPSolver())
        branch_utilization = result.get_branch_utilization()

        rated = ~result.scenario.branch['rating'].isnull()

        # Unrated branches
        self.assertTrue(branch_utilization[~rated].isnull().all())

        # Rated branches
        tolerance = 1e-4

        self.assertTrue((branch_utilization[rated] >= 0 - tolerance).all())
        self.assertTrue((branch_utilization[rated] <= 1 + tolerance).all())

        self.assertTrue(abs(branch_utilization.loc[1] - 0.676520) < tolerance)
        self.assertTrue(abs(branch_utilization.loc[2] - 0.418307) < tolerance)
        self.assertTrue(abs(branch_utilization.loc[3] - 0.838312) < tolerance)
        self.assertTrue(abs(branch_utilization.loc[7] - 0.051104) < tolerance)
        self.assertTrue(abs(branch_utilization.loc[6] - 1.000001) < tolerance)
        self.assertTrue(abs(branch_utilization.loc[5] - 0.365826) < tolerance)

        # Rated parallel branches
        for parallel_branches in ([-1, 5], [-2, 2], [-3, 7]):
            parallel_branches_util = branch_utilization.loc[parallel_branches]
            self.assertTrue(abs(parallel_branches_util.diff().iloc[1]) < tolerance)

    def test_empty_result(self):
        scenario = TEST_SYSTEM.copy()

        # Enforce infeasibility
        for cap in scenario.injector['cap']:
            cap.p_min = cap.p_max = 0

        result = ht.calc_opf(scenario, solver=QCQPSolver())

        with self.assertRaises(ValueError):
            result.get_dynamic_losses()

        with self.assertRaises(ValueError):
            result.get_total_losses()

        with self.assertRaises(ValueError):
            result.get_total_injection_cost()

        with self.assertRaises(ValueError):
            result.get_branch_utilization()
