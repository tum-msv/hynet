import logging
import unittest

from hynet.test.system import TEST_SYSTEM

_log = logging.getLogger(__name__)


class HybridArchitectureTest(unittest.TestCase):
    _MESSAGE_FAILURE = "The hybrid architecture was detected wrongly."

    def setUp(self):
        self._scenario = TEST_SYSTEM.copy()
        self._scenario.branch['angle_min'] = -40.0
        self._scenario.branch['angle_max'] = +40.0

    def tearDown(self):
        pass

    def test_hybrid_architecture(self):
        self.assertTrue(self._scenario.verify_hybrid_architecture_conditions(),
                        "The hybrid architecture was not detected.")

    def test_topology(self):
        branch = self._scenario.branch.loc[3].copy()
        branch.name = 10
        branch['dst'] = 4
        self._scenario.branch = self._scenario.branch.append(branch)
        self.assertFalse(self._scenario.has_acyclic_subgrids(),
                         "The meshed topology was not detected.")
        self.assertFalse(self._scenario.verify_hybrid_architecture_conditions(),
                         self._MESSAGE_FAILURE)

    def test_negative_series_impedance(self):
        self._scenario.branch.at[3, 'z_bar'] -= 0.005
        self.assertFalse(self._scenario.verify_hybrid_architecture_conditions(),
                         self._MESSAGE_FAILURE)

    def test_zero_series_impedance(self):
        self._scenario.branch.at[1, 'z_bar'] = \
            1j * self._scenario.branch.at[1, 'z_bar'].imag
        self.assertFalse(self._scenario.verify_hybrid_architecture_conditions(),
                         self._MESSAGE_FAILURE)

    def test_shunt_conductance_src(self):
        self._scenario.branch.at[5, 'y_src'] -= 0.005
        self.assertFalse(self._scenario.verify_hybrid_architecture_conditions(),
                         self._MESSAGE_FAILURE)

    def test_shunt_conductance_dst(self):
        self._scenario.branch.at[4, 'y_dst'] -= 0.005
        self.assertFalse(self._scenario.verify_hybrid_architecture_conditions(),
                         self._MESSAGE_FAILURE)

    def test_proper_insulation(self):
        # Proper insulation, as defined in the first paper on the hybrid
        # architecture, can also be violated by a too high line charging...
        self._scenario.branch.at[3, 'y_src'] = \
            2j / abs(self._scenario.branch.at[3, 'z_bar'])
        self.assertFalse(self._scenario.verify_hybrid_architecture_conditions(),
                         self._MESSAGE_FAILURE)

    def test_parallel_ratio_difference(self):
        branch = self._scenario.branch.loc[3].copy()
        branch.name = 10
        branch['rho_src'] += 0.005j
        self._scenario.branch = self._scenario.branch.append(branch)
        self.assertFalse(self._scenario.verify_hybrid_architecture_conditions(),
                         self._MESSAGE_FAILURE)

    def test_angle_difference_limit(self):
        self._scenario.branch.at[1, 'angle_min'] = 0.1
        self.assertFalse(self._scenario.verify_hybrid_architecture_conditions(),
                         self._MESSAGE_FAILURE)
