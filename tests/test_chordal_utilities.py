import unittest

import numpy as np

from hynet.types_ import hynet_int_
from hynet.test.system import TEST_SYSTEM

try:
    from hynet.utilities.chordal import get_chordal_extension_cliques
    CHORDAL_MODULE = True
except ImportError:
    CHORDAL_MODULE = False


class ChordalMatrixUtilitiesTest(unittest.TestCase):

    def setUp(self):
        if not CHORDAL_MODULE:
            self.skipTest("The dependencies for the chordal module are not "
                          "installed.")

    def tearDown(self):
        pass

    def test_chordal_extension_cliques_fukuda(self):
        # This is the exemplary graph in Fig. 2.1 in Fukuda et al., "Exploiting
        # Sparsity in Semidefinite Programming via Matrix Completion I: General
        # Framework," SIAM J. Optimization 11 (6), 2001.
        e_src = np.array([0, 0, 1, 1, 1, 2, 3, 4], dtype=hynet_int_)
        e_dst = np.array([4, 6, 2, 3, 5, 5, 6, 5], dtype=hynet_int_)

        cliques, chords = get_chordal_extension_cliques((e_src, e_dst))

        self.assertTrue(len(cliques) == 5)
        self.assertTrue(set(cliques[0]) == set([0, 3, 6]))
        self.assertTrue(set(cliques[1]) == set([0, 1, 3]))
        self.assertTrue(set(cliques[2]) == set([0, 1, 4]))
        self.assertTrue(set(cliques[3]) == set([1, 4, 5]))
        self.assertTrue(set(cliques[4]) == set([1, 2, 5]))
        self.assertTrue(np.all(np.equal(chords[0], [1, 3, 4])))
        self.assertTrue(np.all(np.equal(chords[1], [0, 0, 1])))

    def test_chordal_extension_cliques_acyclic(self):
        # Acyclic graph
        e_src = TEST_SYSTEM.e_src.to_numpy()
        e_dst = TEST_SYSTEM.e_dst.to_numpy()

        cliques, chords = get_chordal_extension_cliques((e_src, e_dst))

        self.assertTrue(len(cliques) == 7)
        self.assertTrue(set(cliques[0]) == set([0, 4]))
        self.assertTrue(set(cliques[1]) == set([0, 3]))
        self.assertTrue(set(cliques[2]) == set([1, 2]))
        self.assertTrue(set(cliques[3]) == set([0, 1]))
        self.assertTrue(set(cliques[4]) == set([5, 6]))
        self.assertTrue(set(cliques[5]) == set([7, 6]))
        self.assertTrue(set(cliques[6]) == set([8, 9]))
        self.assertTrue(chords[0].size == 0 and chords[1].size == 0)

    def test_chordal_extension_cliques_meshed(self):
        # Create graph with a loop 0-1-2-8-9-5-6-7-4-0
        e_src = np.concatenate((TEST_SYSTEM.e_src.to_numpy(), [4, 2, 5]))
        e_dst = np.concatenate((TEST_SYSTEM.e_dst.to_numpy(), [7, 8, 9]))

        cliques, chords = get_chordal_extension_cliques((e_src, e_dst))

        self.assertTrue(len(cliques) == 8)
        self.assertTrue(set(cliques[0]) == set([0, 3]))
        self.assertTrue(set(cliques[1]) == set([0, 1, 4]))
        self.assertTrue(set(cliques[2]) == set([1, 2, 4]))
        self.assertTrue(set(cliques[3]) == set([2, 4, 8]))
        self.assertTrue(set(cliques[4]) == set([4, 8, 9]))
        self.assertTrue(set(cliques[5]) == set([4, 5, 9]))
        self.assertTrue(set(cliques[6]) == set([4, 5, 6]))
        self.assertTrue(set(cliques[7]) == set([4, 6, 7]))
        self.assertTrue(np.all(np.equal(chords[0], [4, 4, 5, 6, 8, 9])))
        self.assertTrue(np.all(np.equal(chords[1], [1, 2, 4, 4, 4, 4])))

    def test_chordal_extension_cliques_amalgamation(self):
        # Create graph where one connected component equals the PJM system
        # topology and the two other connected components are acyclic
        e_src = np.concatenate((TEST_SYSTEM.e_src.to_numpy(), [2, 3]))
        e_dst = np.concatenate((TEST_SYSTEM.e_dst.to_numpy(), [3, 4]))

        cliques, chords = get_chordal_extension_cliques((e_src, e_dst))

        self.assertTrue(len(cliques) == 6)
        self.assertTrue(set(cliques[0]) == set([0, 3, 4]))
        self.assertTrue(set(cliques[1]) == set([0, 2, 3]))
        self.assertTrue(set(cliques[2]) == set([0, 1, 2]))
        self.assertTrue(set(cliques[3]) == set([5, 6]))
        self.assertTrue(set(cliques[4]) == set([6, 7]))
        self.assertTrue(set(cliques[5]) == set([8, 9]))
        self.assertTrue(np.all(np.equal(chords[0], [2])))
        self.assertTrue(np.all(np.equal(chords[1], [0])))

        # REMARK: With one additional chord, i.e., 1-3, the vertices
        # {0, 1, 2, 3} form a maximal clique. With two more additional chords,
        # i.e., 1-4 and 2-4, the chordal extension of the PJM topology becomes
        # a maximal clique.
        cliques, chords = \
            get_chordal_extension_cliques((e_src, e_dst),
                                          amalgamation_size=1,
                                          amalgamation_fill=2)

        # Every connected component is now a maximal clique in the extension
        self.assertTrue(len(cliques) == 3)
        self.assertTrue(set(cliques[0]) == set([0, 1, 2, 3, 4]))
        self.assertTrue(set(cliques[1]) == set([5, 6, 7]))
        self.assertTrue(set(cliques[2]) == set([8, 9]))
        self.assertTrue(np.all(np.equal(chords[0], [2, 3, 4, 4, 7])))
        self.assertTrue(np.all(np.equal(chords[1], [0, 1, 1, 2, 5])))
