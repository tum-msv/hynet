import unittest

import numpy as np

import hynet as ht
from hynet import BusType, InjectorType
from hynet.test.system import TEST_SYSTEM
from hynet.data.connection import DBTransaction
from hynet.data.structure import DBScenarioInactivity, DBShunt
from tests.test_utilities import create_sample_database, remove_temporary_file

TEST_DB_PATH = "_hynet_test_database.db"


class LoadScenarioTest(unittest.TestCase):

    def setUp(self):
        remove_temporary_file(TEST_DB_PATH)
        create_sample_database(TEST_DB_PATH)
        self.database = ht.connect(TEST_DB_PATH)

    def tearDown(self):
        self.database = None
        remove_temporary_file(TEST_DB_PATH)

    def test_load_nonexistent_scenario(self):
        with self.assertRaises(ValueError):
            # Loading non existent scenario should raise error
            ht.load_scenario(self.database, scenario_id=-1)

    def test_exemplary_scenario_1(self):
        scenario = ht.load_scenario(self.database, scenario_id=1)
        self.assert_scenario_data(scenario)

    def test_exemplary_scenario_2(self):
        scenario = ht.load_scenario(self.database, scenario_id=2)
        self.assert_scenario_data(scenario)

    def assert_scenario_data(self, scenario_data):
        self.assertEqual(len(scenario_data.bus), 3, "Invalid number of buses.")
        self.assertEqual(len(scenario_data.branch), 1,
                         "Invalid number of branches.")
        self.assertTrue(scenario_data.name, "Scenario name is missing.")
        self.assertGreater(scenario_data.time, 0, "Invalid scenario time.")

        self.assertEqual(BusType,
                         type(scenario_data.bus['type'].to_numpy()[0]),
                         "BusType type not correct.")
        self.assertEqual(InjectorType,
                         type(scenario_data.injector['type'].to_numpy()[0]),
                         "InjectorType type not correct.")

        self.assertTrue(
            np.all(scenario_data.bus[scenario_data.bus['type']
                                     == BusType.DC]['y_tld'] == 0),
            "DC buses have a shunt.")
        self.assertTrue(
            np.all(scenario_data.bus[scenario_data.bus['type']
                                     == BusType.AC]['y_tld'] != 0),
            "AC buses don't have shunt.")

        self.assertTrue(np.all(scenario_data.bus['load'] != 0),
                        "Some loads are missing.")

        scenario_data.verify()


class ScenarioInactivityTest(unittest.TestCase):

    def setUp(self):
        remove_temporary_file(TEST_DB_PATH)

    def tearDown(self):
        remove_temporary_file(TEST_DB_PATH)

    @staticmethod
    def set_up_database():
        remove_temporary_file(TEST_DB_PATH)
        database = ht.connect(TEST_DB_PATH)
        ht.initialize_database(database, TEST_SYSTEM.copy())
        return database

    def test(self):
        database = self.set_up_database()
        shunt_value = 3 + 4j

        scenario_data = ht.load_scenario(database)
        self.assert_scenario_data(scenario_data)

        with DBTransaction(database) as transaction:
            scenario_inactivity = DBScenarioInactivity()
            scenario_inactivity.scenario_id = 0
            scenario_inactivity.entity_id = 1
            scenario_inactivity.entity_type = 'bus'
            transaction.add(scenario_inactivity)

        # Test with bus removed
        scenario_data = ht.load_scenario(database)
        self.assert_scenario_data(scenario_data,
                                  num_buses=10,
                                  num_branches=6,
                                  num_injectors=5,
                                  num_converters=4)

        database = self.set_up_database()
        with DBTransaction(database) as transaction:
            scenario_inactivity = DBScenarioInactivity()
            scenario_inactivity.scenario_id = 0
            scenario_inactivity.entity_id = 1
            scenario_inactivity.entity_type = 'injector'
            transaction.update(scenario_inactivity)

        # Test with injector removed
        scenario_data = ht.load_scenario(database)
        self.assert_scenario_data(scenario_data,
                                  num_injectors=5)

        database = self.set_up_database()
        with DBTransaction(database) as transaction:
            scenario_inactivity = DBScenarioInactivity()
            scenario_inactivity.scenario_id = 0
            scenario_inactivity.entity_id = 1
            scenario_inactivity.entity_type = 'converter'
            transaction.update(scenario_inactivity)

        # Test with converter removed
        scenario_data = ht.load_scenario(database)
        self.assert_scenario_data(scenario_data,
                                  num_converters=4)

        database = self.set_up_database()

        # Add a shunt
        with DBTransaction(database) as transaction:
            shunt = DBShunt()
            shunt.bus_id = 5
            shunt.p = shunt_value.real
            shunt.q = shunt_value.imag
            shunt.annotation = 'This is an artificial shunt.'
            transaction.add(shunt)

        scenario_data = ht.load_scenario(database)
        self.assert_scenario_data(scenario_data)
        self.assertEqual(shunt_value / scenario_data.base_mva,
                         scenario_data.bus.loc[5, 'y_tld'],
                         'Shunt was not activated.')

        with DBTransaction(database) as transaction:
            scenario_inactivity = DBScenarioInactivity()
            scenario_inactivity.scenario_id = 0
            scenario_inactivity.entity_id = 3  # Two were already present...
            scenario_inactivity.entity_type = 'shunt'
            transaction.update(scenario_inactivity)

        scenario_data = ht.load_scenario(database)
        self.assert_scenario_data(scenario_data)
        self.assertEqual(0j, scenario_data.bus.loc[5, 'y_tld'],
                         'Shunt was not deactivated.')

        with DBTransaction(database) as transaction:
            scenario_inactivity = \
                transaction.query(DBScenarioInactivity)\
                    .filter(DBScenarioInactivity.scenario_id == 0)\
                    .filter(DBScenarioInactivity.entity_id == 3)\
                    .filter(DBScenarioInactivity.entity_type == 'shunt').one()
            scenario_inactivity.scenario_id = 1
            transaction.update(scenario_inactivity)

        scenario_data = ht.load_scenario(database)
        self.assert_scenario_data(scenario_data)
        self.assertEqual(shunt_value / scenario_data.base_mva,
                         scenario_data.bus.loc[5, 'y_tld'],
                         'Shunt was not activated again.')

        with DBTransaction(database) as transaction:
            scenario_inactivity = \
                transaction.query(DBScenarioInactivity)\
                    .filter(DBScenarioInactivity.scenario_id == 1)\
                    .filter(DBScenarioInactivity.entity_id == 3)\
                    .filter(DBScenarioInactivity.entity_type == 'shunt').one()
            scenario_inactivity.entity_type = 'bus'
            transaction.update(scenario_inactivity)

        scenario_data = ht.load_scenario(database)
        # This will fail if bus deactivates unexpectedly
        self.assert_scenario_data(scenario_data)

    def assert_scenario_data(self, scenario_data,
                             num_buses=11,
                             num_branches=10,
                             num_injectors=6,
                             num_converters=5):
        self.assertEqual(num_buses, len(scenario_data.bus),
                         "Number of buses not equal")
        self.assertEqual(num_branches, len(scenario_data.branch),
                         "Number of branches not equal")
        self.assertEqual(num_injectors, len(scenario_data.injector),
                         'Number of injectors not equal')
        self.assertEqual(num_converters, len(scenario_data.converter),
                         'Number of converters not equal')
