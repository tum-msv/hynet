import unittest

from hynet.visual.capability.visualizer import Window
from hynet.scenario.capability import CapRegion, HalfSpace


class CapabilityRegionVisualizerPolygonVerticesTest(unittest.TestCase):

    def setUp(self):
        self.region = CapRegion([-300, 300], [-225, 225],
                                rt=HalfSpace(0.5, -1),
                                rb=HalfSpace(0.5, 1),
                                lb=HalfSpace(0.5, -1),
                                lt=HalfSpace(0.5, 1))

    def tearDown(self):
        self.region = None

    def test_no_halfspaces(self):
        self.region.rt = None
        self.region.rb = None
        self.region.lt = None
        self.region.lb = None

        vertices = Window.compute_polygon_vertices(self.region)

        self.assertEqual(len(vertices), 4)
        self.assertSetEqual(set(vertices),
                            {(+300, +225), (+300, -225),
                             (-300, -225), (-300, +225)})

    def test_all_halfspaces(self):
        vertices = Window.compute_polygon_vertices(self.region)

        self.assertEqual(len(vertices), 8)
        self.assertEqual(vertices[0], (-300, 112.5))
        self.assertEqual(vertices[1], (-187.5, 225))
        self.assertEqual(vertices[2], (187.5, 225))
        self.assertEqual(vertices[3], (300, 112.5))
        self.assertEqual(vertices[4], (300, -112.5))
        self.assertEqual(vertices[5], (187.5, -225))
        self.assertEqual(vertices[6], (-187.5, -225))
        self.assertEqual(vertices[7], (-300, -112.5))

    def test_bottom_halfspaces(self):
        self.region.rt = None
        self.region.lt = None

        vertices = Window.compute_polygon_vertices(self.region)

        self.assertEqual(len(vertices), 6)
        self.assertEqual(vertices[0], (-300, 225))
        self.assertEqual(vertices[1], (300, 225))
        self.assertEqual(vertices[2], (300, -112.5))
        self.assertEqual(vertices[3], (187.5, -225))
        self.assertEqual(vertices[4], (-187.5, -225))
        self.assertEqual(vertices[5], (-300, -112.5))

    def test_top_halfspaces(self):
        self.region.rb = None
        self.region.lb = None

        vertices = Window.compute_polygon_vertices(self.region)

        self.assertEqual(len(vertices), 6)
        self.assertEqual(vertices[0], (-300, 112.5))
        self.assertEqual(vertices[1], (-187.5, 225))
        self.assertEqual(vertices[2], (187.5, 225))
        self.assertEqual(vertices[3], (300, 112.5))
        self.assertEqual(vertices[4], (300, -225))
        self.assertEqual(vertices[5], (-300, -225))

    def test_inactive_q_min_bound(self):
        self.region.rt = None
        self.region.rb = None
        self.region.lt = None
        self.region.lb = HalfSpace(0.5, -.1)

        vertices = Window.compute_polygon_vertices(self.region)

        self.assertEqual(len(vertices), 5)
        self.assertSetEqual(set(vertices),
                            {(+300, +225), (+300, -172.5),
                             (-300, +225), (-300, -112.5)})
