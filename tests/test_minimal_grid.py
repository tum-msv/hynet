import unittest

import numpy as np

import hynet as ht
from tests.data.minimal_grid import get_scenario


class OptimalPowerFlowMinimalGridTest(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test(self):
        for solver_class in ht.AVAILABLE_SOLVERS:
            self.analyze_minimal_grid_opf(solver_class())

    def analyze_minimal_grid_opf(self, solver):
        result = ht.calc_opf(get_scenario(), solver=solver)

        self.assertTrue(len(result.bus.index) == 1)
        self.assertTrue(len(result.branch.index) == 0)
        self.assertTrue(len(result.converter.index) == 0)
        self.assertTrue(len(result.injector.index) == 1)

        self.assertTrue(result.solver_status == ht.SolverStatus.SOLVED)

        total_load = result.scenario.bus['load'].sum()
        total_injection = result.injector['s'].sum()
        shunt = result.scenario.bus['y_tld'].iat[0].conj() \
                    * abs(result.bus['v'].iat[0]) ** 2 \
                    * result.scenario.base_mva
        self.assertTrue(np.isclose(total_injection, total_load + shunt))

        self.assertTrue(np.isclose(result.get_total_losses(), 0.0))

        cost_function = result.scenario.injector['cost_p'].iat[0]
        total_cost = cost_function.evaluate(total_injection.real)
        self.assertTrue(np.isclose(total_cost, result.optimal_value))
        self.assertTrue(np.isclose(total_cost, result.get_total_injection_cost()))

        np.all(np.isclose(result.bus['dv_bal_p'].to_numpy(),
                          cost_function.evaluate(1)))
