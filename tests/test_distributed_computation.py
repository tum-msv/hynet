import logging
import subprocess
import unittest

import hynet as ht
from hynet.test.system import TEST_SYSTEM

_log = logging.getLogger(__name__)


class DistributedComputationTest(unittest.TestCase):

    def setUp(self):
        self.num_jobs = 4
        self.scenarios = []
        for i in range(self.num_jobs):
            self.scenarios.append(TEST_SYSTEM.copy())
            self.scenarios[-1].id = i

    def tearDown(self):
        self.scenarios = None

    def test_local_mode(self):
        _log.info("Starting the server in local mode.")
        server = ht.start_optimization_server(local=True)

        _log.info("Calculating {0} optimal power flows.".format(self.num_jobs))
        results = server.calc_jobs(self.scenarios)

        _log.info("Job completed. Evaluating results.")
        self.check_results(results)

        _log.info("Shutting down the server.")
        server.shutdown()

    def test_optimization_client(self):
        _log.info("Starting the server.")
        server = ht.start_optimization_server()

        _log.info("Starting the client.")
        process = subprocess.Popen('python -m hynet client 127.0.0.1',
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   shell=True)

        _log.info("Calculating {0} optimal power flows.".format(self.num_jobs))
        results = server.calc_jobs(self.scenarios)

        _log.info("Job completed. Evaluating results.")
        self.check_results(results)

        _log.info("Shutting down the server.")
        server.shutdown()

        _log.info("Completed shutdown, waiting for client exit.")
        process.wait()

    def test_qcqp(self):
        qcqps = list(map(lambda x: ht.OPFModel(x).get_problem(),
                         self.scenarios))

        _log.info("Starting the server in local mode.")
        server = ht.start_optimization_server(local=True)

        _log.info("Calculating {0} QCQPs.".format(self.num_jobs))
        results = server.calc_jobs(qcqps)

        _log.info("Job completed. Evaluating results.")
        for result in results:
            self.assertIsInstance(result, ht.QCQPResult)
            self.assertTrue(result.solver_status == ht.SolverStatus.SOLVED)

        _log.info("Shutting down the server.")
        server.shutdown()

    def check_results(self, results):
        for i in range(len(self.scenarios)):
            self.assertIsInstance(results[i], ht.OPFResult,
                                  "Result {0} is not an OPF result".format(i))
            self.assertEqual(results[i].solver_status,
                             ht.SolverStatus.SOLVED,
                             "OPF {0} was not solved successfully.".format(i))
            self.assertEqual(results[i].scenario.id, self.scenarios[i].id,
                             "Ordering of the results is incorrect.")
