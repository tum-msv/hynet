import logging
import unittest

import numpy as np

import hynet as ht
from hynet.solver.ipopt import QCQPSolver
from hynet.test.system import TEST_SYSTEM

_log = logging.getLogger(__name__)


class MaximumLoadabilitySolutionTest(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_fixed_converter_modes(self):
        # Create a scenario with a mixture of forward and backward modes active
        scenario = TEST_SYSTEM.copy()
        self.swap_converter_direction(scenario.converter, id_=2)

        # Disable the loss error check
        result_err = ht.calc_loadability(scenario,
                                         solver=QCQPSolver(),
                                         converter_loss_error_tolerance=np.nan)
        self.assertFalse(result_err.empty)
        self.assertFalse('loss_err_pre' in result_err.converter.columns)

        fwd_mode = (result_err.converter['p_src'] > 0)
        bwd_mode = (result_err.converter['p_dst'] > 0)

        self.assertTrue(not fwd_mode.all())  # At least one in forward mode
        self.assertTrue(not bwd_mode.all())  # At least one in backward mode
        self.assertTrue((fwd_mode | bwd_mode).all())  # No conv. at zero flow

        # Enable the fixing of converter modes with the default tolerance
        result_fix = ht.calc_loadability(scenario,
                                         solver=QCQPSolver(),
                                         converter_loss_error_tolerance=5e-4)
        self.assertFalse(result_fix.empty)
        self.assertTrue('loss_err_pre' in result_fix.converter.columns)

        self.assertTrue((result_fix.converter['loss_err'].abs() < 1e-8).all())

        # Check if the loss error of the pre-solution matches the original one
        self.assertTrue(
        ((
            result_fix.converter['loss_err_pre'] - result_err.converter['loss_err']
        ).abs() < 1e-8).all())

        # Check if the fixed modes match the active ones of the pre-solution
        self.assertTrue((result_fix.converter.loc[fwd_mode, 'p_src'] >= 0).all())
        self.assertTrue((result_fix.converter.loc[bwd_mode, 'p_src'] <= 0).all())

        self.assertTrue((result_fix.converter.loc[bwd_mode, 'p_dst'] >= 0).all())
        self.assertTrue((result_fix.converter.loc[fwd_mode, 'p_dst'] <= 0).all())

        # Check if the flows are similar (They should not change a lot...)
        self.assertTrue(
            (result_err.converter['p_src'] - result_fix.converter['p_src']).abs().sum()
                / result_fix.converter['p_src'].abs().sum() < 0.005
        )
        self.assertTrue(
            (result_err.converter['p_dst'] - result_fix.converter['p_dst']).abs().sum()
                / result_fix.converter['p_dst'].abs().sum() < 0.005
        )

    def test_default_load_increment(self):
        scenario = TEST_SYSTEM.copy()
        self.assertFalse('load_increment' in scenario.bus.columns)
        self.evaluate_maximum_loading(scenario, 0.0075)

    def test_custom_load_increment(self):
        scenario = TEST_SYSTEM.copy()
        scenario.bus['load_increment'] = 0.0
        scenario.bus.loc[2:3, 'load_increment'] = scenario.bus.loc[2:3, 'load']
        self.evaluate_maximum_loading(scenario, 0.33)

    def evaluate_maximum_loading(self, scenario, increment_threshold):
        result = ht.calc_loadability(scenario, solver=QCQPSolver())
        self.assertTrue('load_increment' in scenario.bus.columns)
        self.assertTrue(result.is_valid)
        self.assertTrue(result.load_increment_scaling >= increment_threshold)

        max_load = scenario.bus['load']\
            + result.load_increment_scaling * scenario.bus['load_increment']
        self.assertTrue(
            (result.scenario.bus['load'] - max_load).abs().max() < 1e-8)

    @staticmethod
    def swap_converter_direction(converter, id_):
        converter.loc[id_, ['dst', 'src']] = \
            converter.loc[id_, ['src', 'dst']].to_numpy()
        converter.loc[id_, ['cap_dst', 'cap_src']] = \
            converter.loc[id_, ['cap_src', 'cap_dst']].to_numpy()
        converter.loc[id_, ['loss_bwd', 'loss_fwd']] = \
            converter.loc[id_, ['loss_fwd', 'loss_bwd']].to_numpy()
