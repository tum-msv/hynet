import unittest

import numpy as np

from hynet.types_ import hynet_int_, hynet_eps
from hynet.utilities.graph import (get_adjacency_matrix,
                                   get_laplacian_matrix,
                                   get_minimum_spanning_tree,
                                   get_num_spanning_trees,
                                   is_acyclic_component)
from hynet.test.system import TEST_SYSTEM


class GraphUtilitiesTest(unittest.TestCase):

    def setUp(self):
        # This is the exemplary graph in Fig. 2.1 in Fukuda et al., "Exploiting
        # Sparsity in Semidefinite Programming via Matrix Completion I: General
        # Framework," SIAM J. Optimization 11 (6), 2001.
        e_src = np.array([0, 0, 1, 1, 1, 2, 3, 4], dtype=hynet_int_)
        e_dst = np.array([4, 6, 2, 3, 5, 5, 6, 5], dtype=hynet_int_)
        self.edges = (e_src, e_dst)

    def tearDown(self):
        pass

    def test_adjacency_matrix(self):
        A = get_adjacency_matrix(self.edges)

        #                  0  1  2  3  4  5  6
        A_ref = np.array([[0, 0, 0, 0, 1, 0, 1],  # 0
                          [0, 0, 1, 1, 0, 1, 0],  # 1
                          [0, 1, 0, 0, 0, 1, 0],  # 2
                          [0, 1, 0, 0, 0, 0, 1],  # 3
                          [1, 0, 0, 0, 0, 1, 0],  # 4
                          [0, 1, 1, 0, 1, 0, 0],  # 5
                          [1, 0, 0, 1, 0, 0, 0]   # 6
                          ], dtype=hynet_int_)
        self.assertTrue(np.all(A == A_ref))

        with self.assertRaises(ValueError):
            A = get_adjacency_matrix(self.edges, num_nodes=6)

        A = get_adjacency_matrix(self.edges, num_nodes=8).toarray()
        self.assertTrue(np.all(A[:7, :7] == A_ref))
        self.assertTrue(np.all(A[-1, :] == 0))
        self.assertTrue(np.all(A[:, -1] == 0))

    def test_weighted_adjacency_matrix(self):
        A = get_adjacency_matrix(self.edges, weights=0.1*(np.arange(8) + 1))

        #                   0    1    2    3    4    5    6
        A_ref = np.array([[0.0, 0.0, 0.0, 0.0, 0.1, 0.0, 0.2],  # 0
                          [0.0, 0.0, 0.3, 0.4, 0.0, 0.5, 0.0],  # 1
                          [0.0, 0.3, 0.0, 0.0, 0.0, 0.6, 0.0],  # 2
                          [0.0, 0.4, 0.0, 0.0, 0.0, 0.0, 0.7],  # 3
                          [0.1, 0.0, 0.0, 0.0, 0.0, 0.8, 0.0],  # 4
                          [0.0, 0.5, 0.6, 0.0, 0.8, 0.0, 0.0],  # 5
                          [0.2, 0.0, 0.0, 0.7, 0.0, 0.0, 0.0]   # 6
                          ])
        self.assertTrue(np.max(np.abs(A - A_ref)) < 1e3*hynet_eps)

    def test_laplacian_matrix(self):
        L = get_laplacian_matrix(self.edges)

        #                   0   1   2   3   4   5   6
        L_ref = np.array([[ 2,  0,  0,  0, -1,  0, -1],  # 0
                          [ 0,  3, -1, -1,  0, -1,  0],  # 1
                          [ 0, -1,  2,  0,  0, -1,  0],  # 2
                          [ 0, -1,  0,  2,  0,  0, -1],  # 3
                          [-1,  0,  0,  0,  2, -1,  0],  # 4
                          [ 0, -1, -1,  0, -1,  3,  0],  # 5
                          [-1,  0,  0, -1,  0,  0,  2]   # 6
                          ], dtype=hynet_int_)

        self.assertTrue(np.all(L == L_ref))

    def test_minimum_spanning_tree(self):
        edges = get_minimum_spanning_tree(self.edges, 0.1*(np.arange(8) + 1))

        edges_ref = (np.array([0, 0, 1, 1, 1, 3], dtype=hynet_int_),
                     np.array([4, 6, 2, 3, 5, 6], dtype=hynet_int_))

        self.assertTrue(np.all(edges[0] == edges_ref[0]))
        self.assertTrue(np.all(edges[1] == edges_ref[1]))

    def test_num_spanning_trees(self):
        # Extract the AC branches of the major AC subgrid of the test system
        edges = (TEST_SYSTEM.e_src.iloc[:4].to_numpy(),
                 TEST_SYSTEM.e_dst.iloc[:4].to_numpy())
        self.assertTrue(get_num_spanning_trees(edges) == 1)

        # Add a loop that comprises 4 edges
        edges = (np.concatenate((edges[0], [2])),
                 np.concatenate((edges[1], [3])))
        self.assertTrue(get_num_spanning_trees(edges) == 4)

        # Add another loop to obtain the topology of the PJM test system
        edges = (np.concatenate((edges[0], [3])),
                 np.concatenate((edges[1], [4])))
        self.assertTrue(get_num_spanning_trees(edges) == 11)


    def test_is_acyclic_component(self):
        # Extract the branches of the test system
        edges = (TEST_SYSTEM.e_src.to_numpy(), TEST_SYSTEM.e_dst.to_numpy())
        nodes = np.arange(TEST_SYSTEM.num_buses)

        # Components:
        #  1) Nodes 0, 1, 2, 3, 4
        #  2) Nodes 5, 6, 7
        #  3) Nodes 8, 9
        #  4) Nodes 10

        self.assertTrue(is_acyclic_component(nodes, edges, root=2))
        self.assertTrue(is_acyclic_component(nodes, edges, root=6))
        self.assertTrue(is_acyclic_component(nodes, edges, root=9))
        self.assertTrue(is_acyclic_component(nodes, edges, root=10))

        # Add a loop to component 1 and 2
        edges = (np.concatenate((edges[0], [2], [7])),
                 np.concatenate((edges[1], [3], [5])))

        self.assertFalse(is_acyclic_component(nodes, edges, root=2))
        self.assertFalse(is_acyclic_component(nodes, edges, root=6))
        self.assertTrue(is_acyclic_component(nodes, edges, root=9))
        self.assertTrue(is_acyclic_component(nodes, edges, root=10))
