import unittest

import pandas as pd
from sqlalchemy import delete
from sqlalchemy.sql.elements import and_

import hynet as ht
from hynet.test.system import TEST_SYSTEM
from hynet.data.structure import SCHEMA_VERSION, DBBus
from hynet.data.interface import get_max_scenario_id
from hynet.data.connection import DBTransaction
from hynet.data.structure import (DBScenarioLoad,
                                  DBScenarioInjector,
                                  DBScenarioInactivity,
                                  DBScenario)
from tests.test_utilities import create_sample_database, remove_temporary_file

TEST_DB_PATH = "_hynet_test_database.db"
TEST_DB_PATH_COPY = "_hynet_test_database_copy.db"


class CreateDatabaseTest(unittest.TestCase):

    def setUp(self):
        remove_temporary_file(TEST_DB_PATH)

    def tearDown(self):
        remove_temporary_file(TEST_DB_PATH)

    def test_create_and_transaction(self):
        database = ht.connect(TEST_DB_PATH)

        self.assertEqual(database.version, SCHEMA_VERSION,
                         "The database version entry is invalid.")

        # Insert a dummy bus for test
        with DBTransaction(database) as transaction:
            test_bus = DBBus()
            test_bus.type = ht.BusType.AC
            test_bus.ref = False
            test_bus.base_kv = 1.0
            test_bus.v_min = 42
            test_bus.v_max = 1337
            test_bus.annotation = "This is the test bus!"
            transaction.add(test_bus)

        # Update the dummy bus
        with DBTransaction(database) as transaction:
            # Find the newly added bus
            db_bus = transaction.query(DBBus).first()

            # Check it was inserted correctly
            self.assertEqual(db_bus.v_max, 1337, "v_max value differs")

            db_bus.v_max = 12345
            transaction.update(db_bus)

        # Delete the dummy bus
        with DBTransaction(database) as transaction:
            db_bus = transaction.query(DBBus).first()

            self.assertEqual(db_bus.v_max, 12345, "updated v_max value differs")

            transaction.delete(db_bus)

        with DBTransaction(database) as transaction:
            db_bus = transaction.query(DBBus).first()
            self.assertIsNone(db_bus, "Bus deletion not successful")


class DatabaseInterfaceTest(unittest.TestCase):

    def setUp(self):
        remove_temporary_file(TEST_DB_PATH)
        remove_temporary_file(TEST_DB_PATH_COPY)

    def tearDown(self):
        remove_temporary_file(TEST_DB_PATH)
        remove_temporary_file(TEST_DB_PATH_COPY)

    def test_db_settings(self):
        database = ht.connect(TEST_DB_PATH)
        self.assertTrue(database.grid_name == '')
        self.assertTrue(database.description == '')

        with self.assertRaises(ValueError):
            database.grid_name = 12345

        # Here, we create the DB info entries...
        database.grid_name = "Test Grid"
        database.description = "Test Description"

        self.assertTrue(database.grid_name == "Test Grid")
        self.assertTrue(database.description == "Test Description")

        # ...and here we modify them
        database.grid_name = "Another Grid"
        database.description = "Another Description"

        self.assertTrue(database.grid_name == "Another Grid")
        self.assertTrue(database.description == "Another Description")

    def test_init_load_save_copy(self):
        scenario = TEST_SYSTEM.copy()

        # # REMARK: The test also works with the PJM system if the scaling of
        # # 'cost_p' is commented.
        # database = ht.connect("../hynet-databases/pjm_hybrid.db")
        # scenario = ht.load_scenario(database, scenario_id=0)

        scenario.name = "Save Scenario Test"
        scenario.bus.at[2, 'y_tld'] = 0.001 + 0.05j

        database_new = ht.connect(TEST_DB_PATH)
        database_new.grid_name = "This name will be replaced"
        database_new.description = "This description will be replaced"
        ht.initialize_database(database_new, scenario)
        self.assertTrue(len(ht.get_scenario_info(database_new).index) == 1)
        self.assertTrue(database_new.grid_name == scenario.grid_name)
        self.assertTrue(
            database_new.description.replace('\n', ' ') == scenario.description)

        scenario_new = ht.load_scenario(database_new, scenario_id=0)
        self.assertTrue(scenario_new == scenario)

        scenario.remove_buses([4])
        scenario.injector.drop(1, axis='index', inplace=True)
        scenario.bus.at[2, 'load'] = 50 + 20j
        scenario.bus.at[2, 'y_tld'] = 0j
        scenario.injector.at[2, 'cost_p'].scale(1.5)
        scenario.injector.at[2, 'cost_q'].scale(1.5)
        scenario.injector.at[4, 'cap'].p_max = 700

        ht.save_scenario(database_new, scenario, auto_id=True)
        self.assertTrue(len(ht.get_scenario_info(database_new).index) == 2)

        scenario_new = ht.load_scenario(database_new, scenario_id=1)
        self.assertTrue(scenario_new == scenario)

        # Excursion to test the copying of scenarios...
        database_copy = ht.connect(TEST_DB_PATH_COPY)
        ht.initialize_database(database_copy,
                               ht.load_scenario(database_new, scenario_id=0))
        ht.copy_scenarios(database_new, database_copy)
        scenario_copy = ht.load_scenario(database_copy, scenario_id=1)
        scenario_copy.database_uri = scenario_new.database_uri  # For comparison
        self.assertTrue(scenario_new == scenario_copy)
        # ...end of the excursion :)

        ht.remove_scenarios(database_new, [0, 1])
        self.assertTrue(ht.get_scenario_info(database_new).empty)


class CopyScenariosTest(unittest.TestCase):

    def setUp(self):
        remove_temporary_file(TEST_DB_PATH)
        remove_temporary_file(TEST_DB_PATH_COPY)

    def tearDown(self):
        remove_temporary_file(TEST_DB_PATH)
        remove_temporary_file(TEST_DB_PATH_COPY)

    def test(self):
        create_sample_database(TEST_DB_PATH)
        create_sample_database(TEST_DB_PATH_COPY)

        database_src = ht.connect(TEST_DB_PATH)
        database_dst = ht.connect(TEST_DB_PATH_COPY)

        with DBTransaction(database_dst) as transaction:
            transaction.execute(delete(DBScenarioLoad))
            transaction.execute(delete(DBScenarioInjector))
            transaction.execute(delete(DBScenarioInactivity))
            transaction.execute(delete(DBScenario))

        self.assertIsNone(get_max_scenario_id(database_dst),
                          "Failed to delete scenarios.")

        ht.copy_scenarios(database_src, database_dst)

        self.assertEqual(2, get_max_scenario_id(database_dst),
                         "Failed to copy scenarios (w/o merge).")
        with DBTransaction(database_dst) as transaction:
            self.assertAlmostEqual(0.01,
                                   self.query_scenario_load(1, 1, transaction),
                                   "Bus load was not updated correctly (w/o merge).")

        with DBTransaction(database_dst) as transaction:
            transaction.execute(delete(DBScenarioLoad))
            transaction.execute(delete(DBScenarioInjector))
            transaction.execute(delete(DBScenarioInactivity))
            transaction.execute(delete(DBScenario))

        self.assertIsNone(get_max_scenario_id(database_dst),
                          "Failed to delete scenarios.")

        ht.copy_scenarios(database_src, database_dst,
                          bus_id_map=pd.Series([0, 1, 1, 3]))

        self.assertEqual(2, get_max_scenario_id(database_dst),
                         "Failed to copy scenarios (w/ merge).")
        with DBTransaction(database_dst) as transaction:
            self.assertAlmostEqual(0.03, self.query_scenario_load(1, 1, transaction),
                                   msg="Bus load was not updated correctly (w/ merge).")

    @staticmethod
    def query_scenario_load(scenario_id, bus_id, transaction):
        return transaction.query(DBScenarioLoad)\
                .filter(and_(DBScenarioLoad.bus_id == bus_id,
                             DBScenarioLoad.scenario_id == scenario_id)).one().p
