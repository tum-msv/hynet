"""Artificial minimal grid for code testing"""

import numpy as np
from hynet import (hynet_id_,
                   hynet_float_,
                   hynet_complex_,
                   Scenario,
                   BusType,
                   InjectorType,
                   HalfSpace,
                   CapRegion,
                   PWLFunction)


def get_scenario():
    scr = Scenario()

    scr.id = 0
    scr.name = 'default'
    scr.time = 0
    scr.database_uri = 'minimal_grid.db'
    scr.grid_name = 'Minimal test system'
    scr.base_mva = 100

    scr.bus['id'] = np.arange(1, 2, dtype=hynet_id_)
    scr.bus['type'] = np.array([BusType.AC], dtype=BusType)
    scr.bus['ref'] = np.array([True], dtype=bool)
    scr.bus['y_tld'] = np.array([0.05j], dtype=hynet_complex_)
    scr.bus['load'] = np.array([300 + 100j], dtype=hynet_complex_)
    scr.bus['v_min'] = np.array([0.9], dtype=hynet_float_)
    scr.bus['v_max'] = np.array([1.1], dtype=hynet_float_)
    scr.bus.set_index('id', inplace=True)

    scr.injector['id'] = np.arange(1, 2, dtype=hynet_id_)
    scr.injector['bus'] = np.array([1], dtype=hynet_id_)
    scr.injector['type'] = np.array([InjectorType.CONVENTIONAL],
                                    dtype=InjectorType)
    scr.injector['cap'] = np.array([
                                    CapRegion([0, 600], [-450, 450],
                                              rt=HalfSpace(0.5, -1),
                                              rb=HalfSpace(0.5, 1))
                                    ], dtype=CapRegion)
    scr.injector['cost_p'] = np.array([
                                       PWLFunction(((0, 1), (0, 14)))
                                       ], dtype=PWLFunction)
    scr.injector['cost_q'] = np.array([
                                       None
                                       ], dtype=PWLFunction)
    scr.injector.set_index('id', inplace=True)

    return scr
