loader = fetch("network_graph.json")
    .then(response => response.json(),
        reason => d3
            .select("body")
            .append("div")
            .classed("g", true)
            .enter()
            .text("Failed to load JSON graph data: " + reason))
    .then(data => {
        const height = 800;
        const width = 600;

        const scale = d3.scaleOrdinal(d3.schemeCategory10);
        const color = d => {
            switch (Object.getPrototypeOf(d).attr_dict.graph_type) {
                case "bus":
                    return scale(0);
                case "injector":
                    return scale(1);
                case "converter":
                    return scale(2);
                default:
                    return scale(3);
            }
        };
        const drag = simulation => {

            function dragstarted(d) {
                if (!d3.event.active) simulation.alphaTarget(0.3).restart();
                d.fx = d.x;
                d.fy = d.y;
            }

            function dragged(d) {
                d.fx = d3.event.x;
                d.fy = d3.event.y;
            }

            function dragended(d) {
                if (!d3.event.active) simulation.alphaTarget(0);
                d.fx = null;
                d.fy = null;
            }

            return d3.drag()
                .on("start", dragstarted)
                .on("drag", dragged)
                .on("end", dragended);
        };

        console.log(data);
        const links = data.links.map(d => Object.create(d));
        const nodes = data.nodes.map(d => Object.create(d));

        const simulation = d3.forceSimulation(nodes)
            .force("link", d3.forceLink(links).id(d => d.id).distance(200))
            .force("charge", d3.forceManyBody().strength(-500))
            .force("center", d3.forceCenter(width / 2, height / 2))
            .on("tick", ticked);

        const svg_outer = d3.select("body")
            .append("svg")
            .classed("g-container", true)
            //            .attr("preserveAspectRatio", "none")
            //            .attr("viewBox", "0 0 600 400")
            .call(d3.zoom().on("zoom", function () {
                svg.attr("transform", d3.event.transform)
            }));
        const svg = svg_outer
            .append("g")
            .classed("g", true);

        const link = svg.append("g")
            .attr("stroke", "#999")
            .attr("stroke-opacity", 0.6)
            .selectAll("line")
            .data(links)
            .enter().append("line")
            .attr("stroke-width", d => Math.sqrt(d.value));

        const link_label = svg.append("g")
            .attr("stroke", "#0")
            .attr("stroke-opacity", 1)
            .selectAll("text")
            .data(links)
            .enter()
            .append("text")
            .attr("dx", 0)
            .attr("dy", 0)
            .text(d => d.label);

        const node = svg.append("g")
            .attr("stroke", "#fff")
            .attr("stroke-width", 1.5)
            .selectAll("circle")
            .data(nodes)
            .enter().append("circle")
            .attr("r", 25)
            .attr("fill", color)
            .call(drag(simulation))
            .on('mouseover', applyTable);

        node.append("title")
            .text(d => d.label);

        const node_label = svg.append("g")
            .attr("stroke", "#0")
            .attr("stroke-opacity", 1)
            .selectAll("text")
            .data(nodes)
            .enter()
            .append("text")
            .attr("dx", 0)
            .attr("dy", 0)
            .attr("text-anchor", "middle")
            .text(d => d.id);


        function ticked() {
            link
                .attr("x1", d => d.source.x)
                .attr("y1", d => d.source.y)
                .attr("x2", d => d.target.x)
                .attr("y2", d => d.target.y);

            link_label
                .attr("dx", d => 0.5 * (d.source.x + d.target.x))
                .attr("dy", d => 0.5 * (d.source.y + d.target.y));

            node
                .attr("cx", d => d.x)
                .attr("cy", d => d.y);

            node_label
                .attr("dx", d => d.x)
                .attr("dy", d => d.y);
        }

        function applyTable(element) {
            element = Object.getPrototypeOf(element).attr_dict;
            const table = d3.select("#inspector-table").html("");
            let row = table.append("tr");
            row.append("th").text("Key");
            row.append("th").text("Value");
            for (let key in element) {
                if (element.hasOwnProperty(key)) {
                    row = table.append("tr");
                    row.append("td").text(key);
                    row.append("td").text(element[key]);
                }
            }
        }

    })
;
