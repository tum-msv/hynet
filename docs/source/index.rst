.. only:: html

    .. mdinclude:: ../../README.md

.. only:: latex

    Welcome to *hynet*
    ==================

.. toctree::
    :maxdepth: 4
    :hidden:

    readme

.. toctree::
    :maxdepth: 4
    :hidden:

    usage

.. toctree::
    :maxdepth: 4
    :hidden:

    hynet

.. toctree::
    :maxdepth: 4
    :hidden:

    changelog
